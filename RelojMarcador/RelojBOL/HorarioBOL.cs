﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RelojMarcadorENL;
using RelojMarcadorDAL;

namespace RelojMarcadorBOL
{
    public class HorarioBOL
    {
        private HorarioDAL hdal;

        public HorarioBOL()
        {
            hdal = new HorarioDAL();
        }
        /// <summary>
        /// Conecta con El DAL para Agregar
        /// </summary>
        /// <param name="horario">Horario a Agregar</param>
        /// <returns>True:Agrego False: No Agergo </returns>
        public bool Agregar(EHorario horario)
        {
            if (horario.HoraInicio.Equals("") || horario.HoraFinal.Equals(""))
            {
                throw new Exception("Tiempo del Horario Requerido");
            }
            if (horario.Dia.Equals(""))
            {
                throw new Exception("Dia del Horario Requerido");
            }
            if (horario.Id > 0)
            {
                return hdal.Editar(horario);
            }
            else
            {
            return hdal.Agregar(horario);
            }

        }
        /// <summary>
        /// Conecta con El DAL para Cargar la lista de Horarios
        /// </summary>
        /// <returns>Lista de Horarios</returns>
        public List<EHorario> CargaTodos()
        {
            return hdal.CargarHorarios();
        }
        /// <summary>
        /// Conecta con El DAL para Saber si un Horario esta Repetido
        /// </summary>
        /// <param name="ea"> Horario a evaluar</param>
        /// <returns>True:Repetido False: No repetido</returns>
        public bool Repetido(EHorario ea)
        {
            return hdal.EstaRepetido(ea);
        }
        /// <summary>
        /// Conecta con El DAL para Remover
        /// </summary>
        /// <param name="horario">Horario a romever</param>
        /// <returns>True:Removio False: No Removio</returns>
        public bool Remove(EHorario horario)
        {
            return hdal.Remove(horario);
        }
    }
}
