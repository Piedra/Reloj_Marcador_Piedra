﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RelojMarcadorENL;
using RelojMarcadorDAL;

namespace RelojMarcadorBOL
{
    public class CursoBOL
    {
        CursoDAL cdal;

        public CursoBOL()
        {
            cdal = new CursoDAL();

        }
        /// <summary>
        /// LLama al Curso dal Para Crear un nuevo curso y si tiene Un Id lo edita
        /// </summary>
        /// <param name="curso">Curso a crear </param>
        /// <returns>True: Lo Creo False: No lo creo</returns>
        public bool Agregar(ECurso curso)
        {
            if (curso.Nombre.Equals(""))
            {
                throw new Exception("Curso debe llevar un Nombre");
            }
            if (curso.Cupo == 0)
            {
                throw new Exception("El Cupo del Curso debe Ser Mayor a 0");
            }
            if (curso.Aula.Equals(""))
            {
                throw new Exception("Debe indicar el Nombre del Aula");
            }
            if (curso.FechaInicio==null)
            {
                throw new Exception("Curso debe llevar una fecha inicla");
            }
            if (curso.FechaFinal == null)
            {
                throw new Exception("Curso debe llevar una fecha Final");
            }
            if (curso.Horario == null)
            {
                throw new Exception("Seleccione un Horario para el Curso");
            }
            
            if (curso.Id > 0)
            {
                return cdal.Editar(curso);
            }
            else
            {
                return cdal.Agregar(curso);
            }
        }
        /// <summary>
        /// LLama al Curso DAl para Cargar los Cursos 
        /// </summary>
        /// <returns>Lista de Cursos</returns>
        public List<ECurso> CargarTodos()
        {
            return cdal.CargarTodosCursos();
        }
        /// <summary>
        /// LLama al Dal para Remover un Curso
        /// </summary>
        /// <param name="curso">Curso a Remover </param>
        /// <returns>True: Lo Creo Elimino: No lo Elimino</returns>
        public bool Remove(ECurso curso)
        {
            return cdal.Remove(curso);
        }
    }
}
