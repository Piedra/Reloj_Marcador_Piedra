﻿using RelojMarcadorDAL;
using RelojMarcadorENL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RelojMarcadorBOL
{
    public class MarcaBOL
    {

        private MarcaDAL mdal;

        public MarcaBOL()
        {
            mdal = new MarcaDAL();
        }
        /// <summary>
        /// Conecta con el DAL para Preguntar si hay una marca Creada
        /// </summary>
        /// <param name="docente">Docente </param>
        /// <returns>Emarca: Si encuentra Null: si no</returns>
        public EMarca MarcaCreada(EDocente docente)
        {
            return mdal.MarcaCreada(docente);
        }
        /// <summary>
        /// Conecta con DAL para Registrar una Salida
        /// </summary>
        /// <param name="marca">EMarca llena</param>
        /// <returns></returns>
        public bool HacerSalida(EMarca marca)
        {
            return mdal.HacerSalida(marca);
        }
        /// <summary>
        /// conecta con el Dal para Registrar una Entrada
        /// </summary>
        /// <param name="docente">Docente </param>
        /// <param name="introduccion">Introduccion</param>
        /// <returns>True: Creo False: No creo</returns>
        public bool NuevoIngreso(EDocente docente, string introduccion)
        {
            return mdal.NuevoIngreso(docente, introduccion);
        }
        /// <summary>
        /// COnecta con EL DAL para bucar por ID
        /// </summary>
        /// <param name="pin">Pin del Docente</param>
        /// <returns>Lista de Marcas </returns>
        public List<EMarca> BuscarPor(string pin)
        {
            return  mdal.CargarPorPin(pin);
        }
        
    }
}

