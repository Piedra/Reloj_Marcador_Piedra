﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RelojMarcadorDAL;
using RelojMarcadorENL;

namespace RelojMarcadorBOL
{
    public class DocenteBOL
    {
        private DocenteDAL ddal;
        public DocenteBOL()
        {
            ddal = new DocenteDAL();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="docente"></param>
        /// <returns></returns>
        public bool Agregar(EDocente docente)
        {
            Regulaciones(docente);
            if (docente.Id > 0)
            {
                return ddal.Editar(docente);//Aqui va el Editar
            }
            else
            {
                return ddal.Agregar(docente);
            }
        }
        /// <summary>
        /// Regulaciones del Docente para ingresar
        /// </summary>
        /// <param name="docente"> Docente a Ingresar</param>
        private void Regulaciones(EDocente docente)
        {
            if (docente.Pin < 0)
            {
                throw new Exception("Campo PIN  Vacío.");
            }
            if (docente.Pin < 1000)
            {
                throw new Exception("El PIN Debe constar de 4 Digitos, y no debe empezar con 0");

            }

            if (docente.Cedula.Equals(" -    -"))
            {
                throw new Exception("Campo de Cedula Vacío.");

            }

            if (docente.Nombre.Equals(""))
            {
                throw new Exception("Campo del Nombre Vacío.");

            }

            if (docente.Apellido.Equals(""))
            {
                throw new Exception("Campo del Apellido Vacío.");

            }
            if (docente.Email.Equals(""))
            {
                throw new Exception("Campo del Correo Vacío.");

            }
            if (docente.Telefono.Equals(""))
            {
                throw new Exception("Campo del Telefono Vacío");

            }
            if (Convert.ToInt32(docente.Telefono) < 80000000)
            {
                throw new Exception("El numero Telefonico debe Tener 8 Digitos. Si desea Agregar un " +
                    "numero internacional Contactar a Mant.");
            }
            if (docente.Cursos.Count == 0)
            {
                throw new Exception("El Docente Se debe ingresar con cursos.");

            }
        }
        /// <summary>
        /// Conecta con el Dal para Cargar todos los docnetes
        /// </summary>
        /// <returns>Lista de Docentes</returns>
        public List<EDocente> CargaTodos()
        {
            return ddal.CargarDocentes();
        }
        /// <summary>
        /// Conecta con el DAL para remover el Docente
        /// </summary>
        /// <param name="docente">Docente a remover</param>
        /// <returns>Bool: True: Lo Removio False: No lo removio</returns>
        public bool Remove(EDocente docente)
        {
            return ddal.Remove(docente);
        }
        /// <summary>
        /// Conecta con el DAL Para hacer el Login
        /// </summary>
        /// <param name="pin">Pin del docente</param>
        /// <returns>Docente</returns>
        public EDocente Login(string pin)
        {
            return ddal.Login(pin);
        }
        /// <summary>
        /// Agrega una tardia al docente
        /// </summary>
        /// <param name="docente"></param>
        public void AgregarTardia(EDocente docente)
        {
            ddal.AgregarTardia(docente);
        }
        /// <summary>
        /// Conecta con el Dal para Buscar por pin
        /// </summary>
        /// <param name="text">String Pin </param>
        /// <returns>El Docente</returns>
        public EDocente BuscarPorPin(string text)
        {
            return ddal.BuscarPorPin(text);
        }
        /// <summary>
        /// Conecta con el dal para Cargar al Menos Desctada y al mas Destacado
        /// </summary>
        /// <returns>Lista de los dos</returns>
        public List<EDocente> DestcadoMenos()
        {
            return ddal.DestacadoMenos();
        }
    }
}
