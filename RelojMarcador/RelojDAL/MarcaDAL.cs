﻿using RelojMarcadorENL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace RelojMarcadorDAL
{
    public class MarcaDAL
    {
        private string miRutaXml;
        private XmlDocument xmlM;

        public MarcaDAL()
        {
            xmlM = new XmlDocument();
            miRutaXml = @"C:\Users\Fabricio\Documents\Progra III\Ptoyecto3.0\xml\Marcas.xml";
        }
        /// <summary>
        /// Crea un Nueva Marca y lo ingresa Al XML
        /// </summary>
        /// <param name="marca">EMarca: Marca a ingresar </param>
        /// <returns>Bool:True: Lo Ingreso False: No lo ingreso.</returns>
        public bool Agregar(EMarca marca)
        {


            xmlM.Load(miRutaXml);

            XmlNode xMarca = NuevaMarca(marca);

            XmlNode nodoRaiz = xmlM.DocumentElement;

            nodoRaiz.InsertAfter(xMarca, nodoRaiz.LastChild);

            xmlM.Save(miRutaXml);

            return true;

        }
        /// <summary>
        /// Busca una Marca por el Pin de un Docente
        /// </summary>
        /// <param name="pin">String Pin del docente</param>
        /// <returns>Devuelve las Marcas que tiene Ese Docente</returns>
        public List<EMarca> CargarPorPin(string pin)
        {
            DocenteDAL ddal = new DocenteDAL();
            EDocente docente = ddal.BuscarPorPin(pin);
            List<EMarca> marcas = new List<EMarca>();
            xmlM.Load(miRutaXml);
            String id = String.Format("Marcas/Marca");
            XmlNodeList marcasa = xmlM.SelectNodes(id);
            foreach (XmlNode node in marcasa)
            {
                if (node.ChildNodes[1].InnerText == docente.Id.ToString())
                {
                    marcas.Add(CargarMarca(node));
                }
            }
            return marcas;
        }
        /// <summary>
        /// Es una Marca de Ingreso, Llena la primera parte de la Marca del Doncete
        /// </summary>
        /// <param name="docente">Docete que hace la Marca</param>
        /// <param name="introduccion">Introduccion: Mensaje de Introduccion </param>
        /// <returns></returns>
        public XmlNode IngresoMarca(EDocente docente, string introduccion)
        {
            XmlNode xmarca = xmlM.CreateElement("Marca");

            XmlElement xId = xmlM.CreateElement("Id_Marca");
            xId.InnerText = ProximoId().ToString();
            xmarca.AppendChild(xId);

            XmlElement xDoce = xmlM.CreateElement("Id_Docente");
            xDoce.InnerText = docente.Id.ToString();
            xmarca.AppendChild(xDoce);

            XmlElement xEntra = xmlM.CreateElement("Entrada");
            xEntra.InnerText = "1";
            xmarca.AppendChild(xEntra);

            XmlElement xEntH = xmlM.CreateElement("Hora_Entrada");
            xEntH.InnerText = DateTime.Now.ToString("HH:mm");
            xmarca.AppendChild(xEntH);

            XmlElement xInt = xmlM.CreateElement("Introduccion");
            xInt.InnerText = introduccion;
            xmarca.AppendChild(xInt);

            XmlElement xSa = xmlM.CreateElement("Salida");
            xSa.InnerText = "0";
            xmarca.AppendChild(xSa);

            XmlElement xHsa = xmlM.CreateElement("Hora_Salida");
            xHsa.InnerText = "0";
            xmarca.AppendChild(xHsa);

            XmlElement xJus = xmlM.CreateElement("Justificacion");
            xJus.InnerText = "N.E.";
            xmarca.AppendChild(xJus);

            XmlElement xDia = xmlM.CreateElement("Dia");
            xDia.InnerText = DateTime.Now.ToString("dd/MM/yyyy");
            xmarca.AppendChild(xDia);

            return xmarca;
        }
        /// <summary>
        /// Hace la Marca de SALIDA, y termina de llenar los datos
        /// </summary>
        /// <param name="marca">Marca con datos llenos</param>
        /// <returns>Bool para saber si realizo la marcav </returns>
        public bool HacerSalida(EMarca marca)
        {
            xmlM.Load(miRutaXml);
            String id = String.Format("Marcas/Marca");
            XmlNodeList marcas = xmlM.SelectNodes(id);
            foreach (XmlNode node in marcas)
            {
                if (node.FirstChild.InnerText == marca.Id.ToString())
                {
                    node.ChildNodes[5].InnerText = "1";
                    node.ChildNodes[6].InnerText = DateTime.Now.ToString("HH:mm");
                    node.ChildNodes[7].InnerText = marca.Justificacion;
                    xmlM.Save(miRutaXml);
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Hace un nodo A partir de la marca
        /// </summary>
        /// <param name="marca">Marca Nueva</param>
        /// <returns></returns>
        private XmlNode NuevaMarca(EMarca marca)
        {
            XmlNode xmarca = xmlM.CreateElement("Marca");

            XmlElement xId = xmlM.CreateElement("Id_Marca");
            xId.InnerText = ProximoId().ToString();
            xmarca.AppendChild(xId);

            XmlElement xDoce = xmlM.CreateElement("Id_Docente");
            xDoce.InnerText = marca.Id_Docente.ToString();
            xmarca.AppendChild(xDoce);

            XmlElement xEntra = xmlM.CreateElement("Entrada");
            xEntra.InnerText = marca.Entrada.ToString();
            xmarca.AppendChild(xEntra);

            XmlElement xHorE = xmlM.CreateElement("Hora_Entrada");
            xHorE.InnerText = marca.HoraDeEntrada.ToString();
            xmarca.AppendChild(xHorE);

            XmlElement xInt = xmlM.CreateElement("Introduccion");
            xInt.InnerText = marca.Introduccion;
            xmarca.AppendChild(xInt);

            XmlElement xSa = xmlM.CreateElement("Salida");
            xSa.InnerText = marca.Salida.ToString();
            xmarca.AppendChild(xSa);

            XmlElement xHsa = xmlM.CreateElement("Hora_Salida");
            xHsa.InnerText = marca.HoraSalida.ToString();
            xmarca.AppendChild(xHsa);

            XmlElement xJus = xmlM.CreateElement("Justificacion");
            xJus.InnerText = marca.Justificacion;
            xmarca.AppendChild(xJus);

            XmlElement xDia = xmlM.CreateElement("Dia");
            xDia.InnerText = marca.Dia.ToString("dd/MM/yyyy");
            xmarca.AppendChild(xDia);

            return xmarca;
        }
        /// <summary>
        /// Busca el Proximo Id
        /// </summary>
        /// <returns></returns>
        private object ProximoId()
        {
            int id = 1;
            while (true)
            {
                if (BuscarPorID(id.ToString()).Equals(id.ToString()))
                {
                    id++;
                }
                else
                {
                    return id;
                }
            }
        }
        /// <summary>
        /// Busca por id El Proxmo Id
        /// </summary>
        /// <param name="num">Strinmg del Id</param>
        /// <returns>String con el valor del Id</returns>
        private object BuscarPorID(string num)
        {
            string siguiente = "";
            xmlM.Load(miRutaXml);
            String id = String.Format("Marcas/Marca");
            XmlNodeList docentes = xmlM.SelectNodes(id);

            foreach (XmlNode node in docentes)
            {
                if (node.FirstChild.InnerText.Equals(num))
                {
                    siguiente = node.FirstChild.InnerText;
                }
            }
            return siguiente;
        }
        /// <summary>
        /// Carga toas las Marcas
        /// </summary>
        /// <returns>List COn la marcas </returns>
        public List<EMarca> CargarTodasMarcas()
        {
            List<EMarca> marcas = new List<EMarca>();
            xmlM.Load(miRutaXml);
            String id = String.Format("Marcas/Marca");
            XmlNodeList listaDoc = xmlM.SelectNodes(id);
            foreach (XmlNode node in listaDoc)
            {
                if (node.LastChild.InnerText.Equals("1"))
                {
                    marcas.Add(CargarMarca(node));
                }
            }

            return marcas;
        }
        /// <summary>
        /// Toma un Nodo y lo convierte en una Marca
        /// </summary>
        /// <param name="node">Node a ser Marca</param>
        /// <returns>Lista de Marcas</returns>
        private EMarca CargarMarca(XmlNode node)
        {
            Console.WriteLine(node.InnerText);
            
            EMarca ma = new EMarca
            {
                Id = Convert.ToInt32(node.FirstChild.InnerText),
                Id_Docente = Convert.ToInt32(node.ChildNodes[1].InnerText),
                Entrada = Convert.ToInt32(node.ChildNodes[2].InnerText),
                HoraDeEntrada = node.ChildNodes[3].InnerText,
                Introduccion = node.ChildNodes[4].InnerText,
                Salida = Convert.ToInt32(node.ChildNodes[5].InnerText),
                HoraSalida = node.ChildNodes[6].InnerText,
                Justificacion = node.ChildNodes[7].InnerText,
                Dia = DateTime.ParseExact(node.LastChild.InnerText,
                    "dd/MM/yyyy", CultureInfo.InvariantCulture)
            };
            return ma;
        }
        /// <summary>
        /// Busca una Marca por el Id 
        /// </summary>
        /// <param name="idC"></param>
        /// <returns></returns>
        public EMarca BuscarMarcaId(string idC)
        {
            xmlM.Load(miRutaXml);
            String id = String.Format("Marcas/Marca");
            XmlNodeList marcas = xmlM.SelectNodes(id);
            foreach (XmlNode node in marcas)
            {
                if (node.FirstChild.InnerText.Equals(idC))
                {
                    return CargarMarca(node);
                }
            }
            return null;
        }
        /// <summary>
        /// Identifica si una marca ya fue creada el dia de hoy por un docente
        /// </summary>
        /// <param name="docente">Docente </param>
        /// <returns>Marca si asi es Null, si no la encuentra</returns>
        public EMarca MarcaCreada(EDocente docente)
        {
            string day = DateTime.Now.ToString("dd/MM/yyyy");
            xmlM.Load(miRutaXml);
            String id = String.Format("Marcas/Marca");
            XmlNodeList marcas = xmlM.SelectNodes(id);
            foreach (XmlNode node in marcas)
            {
                if (node.ChildNodes[1].InnerText.Equals(docente.Id.ToString()) &&
                    node.LastChild.InnerText.Equals(day))
                {
                    if (node.ChildNodes[2].InnerText == "1" &&
                        node.ChildNodes[5].InnerText == "0")
                    {
                        return CargarMarca(node);
                    }
                    if (node.ChildNodes[2].InnerText == "1" &&
                       node.ChildNodes[5].InnerText == "1")
                    {
                        throw new Exception("Docente Ya Ingreso La Marca del Dia");
                    }
                }
            }
            return null;
        }
        /// <summary>
        /// Hace un Nuevo Ingresoi de un Marca
        /// </summary>
        /// <param name="doc">Docente</param>
        /// <param name="intro">introduccion</param>
        /// <returns></returns>
        public bool NuevoIngreso(EDocente doc, string intro)
        {

            xmlM.Load(miRutaXml);

            XmlNode xMarca = IngresoMarca(doc, intro);

            XmlNode nodoRaiz = xmlM.DocumentElement;

            nodoRaiz.InsertAfter(xMarca, nodoRaiz.LastChild);

            xmlM.Save(miRutaXml);

            return true;
        }
    }
}
