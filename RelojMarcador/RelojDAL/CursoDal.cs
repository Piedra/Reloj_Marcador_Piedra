﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using RelojMarcadorENL;

namespace RelojMarcadorDAL
{
    public class CursoDAL
    {
        private XmlDocument xmlC;
        private string miRutaXml;
        private HorarioDAL hdal;

        public CursoDAL()
        {
            hdal = new HorarioDAL();
            xmlC = new XmlDocument();
            miRutaXml = @"C:\Users\Fabricio\Documents\Progra III\Ptoyecto3.0\xml\Cursos.xml";

        }
        
        /// <summary>
        /// Agrega un Nuevo Curso al XML
        /// </summary>
        /// <param name="curso">Curso a Agregar</param>
        /// <returns>True: si lo Agrego Fale: si no</returns>
        public bool Agregar(ECurso curso)
        {
            if (AulaHoraRepetida(curso))
            {
                throw new Exception("Ya existe un Curso Registrado en esa misma hora " +
                    "y en ese mismo horario");
            }
            else
            {
                xmlC.Load(miRutaXml);

                XmlNode xCurso = NuevoCurso(curso);

                XmlNode nodoRaiz = xmlC.DocumentElement;

                nodoRaiz.InsertAfter(xCurso, nodoRaiz.LastChild);

                xmlC.Save(miRutaXml);

                return true;
            }
        }
        /// <summary>
        /// Edita un Curso
        /// </summary>
        /// <param name="curso">Curso a Modificar</param>
        /// <returns>rue: si lo Agrego Fale: si no</returns>
        public bool Editar(ECurso curso)
        {
            xmlC.Load(miRutaXml);
            String id = String.Format("Cursos/Curso");
            XmlNodeList cursos = xmlC.SelectNodes(id);
            foreach (XmlNode node in cursos)
            {
                if (node.FirstChild.InnerText == curso.Id.ToString())
                {
                    node.ChildNodes[1].InnerText = curso.Nombre;
                    node.ChildNodes[2].InnerText = curso.Aula;
                    node.ChildNodes[3].InnerText = curso.FechaInicio.ToString("dd/MM/yyyy");
                    node.ChildNodes[4].InnerText = curso.FechaFinal.ToString("dd/MM/yyyy");
                    node.ChildNodes[5].InnerText = curso.Cupo.ToString();
                    node.ChildNodes[6].InnerText = curso.Horario.Id.ToString();
                    node.LastChild.InnerText = curso.Activo.ToString();
                    xmlC.Save(miRutaXml);
                    return true;
                }
            }

            return false;
        }
        /// <summary>
        /// Toma un Curso y lo "Elimina" lo Desactiva
        /// </summary>
        /// <param name="curso">Curso a Eliminar</param>
        /// <returns>rue: si lo Agrego Fale: si no</returns>
        public bool Remove(ECurso curso)
        {
            xmlC.Load(miRutaXml);
            String id = String.Format("Cursos/Curso");
            XmlNodeList docentes = xmlC.SelectNodes(id);
            foreach (XmlNode node in docentes)
            {
                if (node.FirstChild.InnerText == curso.Id.ToString())
                {
                    node.LastChild.InnerText = "0";
                    xmlC.Save(miRutaXml);
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Hace un Curso de Node
        /// </summary>
        /// <param name="curso">Node A Cargar</param>
        /// <returns>Curso Cargado</returns>
        private XmlNode NuevoCurso(ECurso curso)
        {
            XmlNode xcurso = xmlC.CreateElement("Curso");

            XmlElement xId = xmlC.CreateElement("Id_Curso");
            xId.InnerText = ProximoId().ToString();
            xcurso.AppendChild(xId);

            XmlElement xNom = xmlC.CreateElement("Nombre");
            xNom.InnerText = curso.Nombre;
            xcurso.AppendChild(xNom);

            XmlElement xAu = xmlC.CreateElement("Aula");
            xAu.InnerText = curso.Aula;
            xcurso.AppendChild(xAu);

            XmlElement xFI = xmlC.CreateElement("Fecha_Inicial");
            xFI.InnerText = curso.FechaInicio.ToString("dd/MM/yyyy");
            xcurso.AppendChild(xFI);

            XmlElement xFF = xmlC.CreateElement("Fecha_Final");
            xFF.InnerText = curso.FechaFinal.ToString("dd/MM/yyyy");
            xcurso.AppendChild(xFF);

            XmlElement xCupo = xmlC.CreateElement("Cupo");
            xCupo.InnerText = curso.Cupo.ToString();
            xcurso.AppendChild(xCupo);

            XmlElement xHo = xmlC.CreateElement("Id_Horario");
            xHo.InnerText = curso.Horario.Id.ToString();
            xcurso.AppendChild(xHo);

            XmlElement xAc = xmlC.CreateElement("Activo");
            xAc.InnerText = "1";
            xcurso.AppendChild(xAc);

            return xcurso;

        }
        /// <summary>
        /// Busca el Proximo Ide del Curso
        /// </summary>
        /// <returns></returns>
        private int ProximoId()
        {
            int id = 1;
            while (true)
            {
                if (BuscarPorID(id.ToString()).Equals(id.ToString()))
                {
                    id++;
                }
                else
                {
                    return id;
                }
            }
        }
        /// <summary>
        /// Busca un Id por un Strin y devuelve el siguiente Numero
        /// </summary>
        /// <param name="num">String del Id</param>
        /// <returns>String del sieguiente Id</returns>
        public string BuscarPorID(string num)
        {
            string siguiente = "";
            xmlC.Load(miRutaXml);
            String id = String.Format("Cursos/Curso");
            XmlNodeList docentes = xmlC.SelectNodes(id);

            foreach (XmlNode node in docentes)
            {
                if (node.FirstChild.InnerText.Equals(num))
                {
                    siguiente = node.FirstChild.InnerText;
                }
            }
            return siguiente;
        }
        /// <summary>
        /// Busca un Curso por ID
        /// </summary>
        /// <param name="idC">Id del curso</param>
        /// <returns>Curso encontrado si no devulve null</returns>
        public ECurso BuscarId(string idC)
        {
            xmlC.Load(miRutaXml);
            String id = String.Format("Cursos/Curso");
            XmlNodeList docentes = xmlC.SelectNodes(id);
            foreach (XmlNode node in docentes)
            {
                if (node.FirstChild.InnerText.Equals(idC))
                {
                    return CargarCurso(node); ;
                }
            }
            return null;
        }
        /// <summary>
        /// Hace un Node de un Curso para Ingresar Al XML
        /// </summary>
        /// <param name="node">Curso a Convertir</param>
        /// <returns>Xml node Cargado</returns>
        private ECurso CargarCurso(XmlNode node)
        {
            ECurso c = new ECurso
            {
                Id = Convert.ToInt32(node.FirstChild.InnerText),
                Nombre = node.ChildNodes[1].InnerText,
                Aula = node.ChildNodes[2].InnerText,
                FechaInicio = DateTime.ParseExact(node.ChildNodes[3].InnerText,
                    "dd/MM/yyyy", CultureInfo.InvariantCulture),
                FechaFinal = DateTime.ParseExact(node.ChildNodes[4].InnerText,
                    "dd/MM/yyyy", CultureInfo.InvariantCulture),
                Cupo = Convert.ToInt32(node.ChildNodes[5].InnerText),
                Horario = hdal.CargarHorarioPorId(node.ChildNodes[6].InnerText),
                Activo = Convert.ToInt32(node.ChildNodes[7].InnerText)
            };
            return c;
        }
        /// <summary>
        /// Carga todos los Cursos
        /// </summary>
        /// <returns>List Cursos</returns>
        public List<ECurso> CargarTodosCursos()
        {
            List<ECurso> cursos = new List<ECurso>();
            xmlC.Load(miRutaXml);
            String id = String.Format("Cursos/Curso");
            XmlNodeList listaDoc = xmlC.SelectNodes(id);
            foreach (XmlNode node in listaDoc)
            {
                if (node.LastChild.InnerText.Equals("1"))
                {
                cursos.Add(CargarCurso(node));
                }
            }
            return cursos;
        }
        /// <summary>
        /// Indentifica si Una Aula Va ser utilizada en Esas mismas fechas y a esa misma Hora
        /// </summary>
        /// <param name="oldCurso"></param>
        /// <returns></returns>
        internal bool AulaHoraRepetida(ECurso oldCurso)
        {
            xmlC.Load(miRutaXml);
            String id = String.Format("Cursos/Curso");
            XmlNodeList cursos = xmlC.SelectNodes(id);

            foreach (XmlNode node in cursos)
            {
                if (node.ChildNodes[2].InnerText.Equals(oldCurso.Aula)&&
                    node.ChildNodes[3].InnerText.Equals(oldCurso.FechaInicio.ToString("dd/MM/yyyy"))&&
                    node.ChildNodes[4].InnerText.Equals(oldCurso.FechaFinal.ToString("dd/MM/yyyy"))&&
                    node.ChildNodes[6].InnerText.Equals(oldCurso.Horario.Id)
                    )
                {
                    return true;
                    
                }
            }
            return false;
        }


        
        
    }
}
