﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using RelojMarcadorENL;

namespace RelojMarcadorDAL
{
    public class DocenteDAL
    {
        private CursoDAL cdal;
        private XmlDocument xmlD;
        private string miRutaXml;

        public DocenteDAL()
        {
            cdal = new CursoDAL();
            xmlD = new XmlDocument();
            miRutaXml = @"C:\Users\Fabricio\Documents\Progra III\Ptoyecto3.0\xml\Docentes.xml";
        }

        /// <summary>
        /// Agrega un Nuevo Docente
        /// </summary>
        /// <param name="docente">Docente a Agregar</param>
        /// <returns>Devuelve un Docente Agregado</returns>
        public bool Agregar(EDocente docente)
        {
            Validaciones(docente);
            xmlD.Load(miRutaXml);
            XmlNode empleado = NuevoDocente(docente);

            XmlNode nodoRaiz = xmlD.DocumentElement;

            nodoRaiz.InsertAfter(empleado, nodoRaiz.LastChild);

            xmlD.Save(miRutaXml);

            return true;

        }
        /// <summary>
        /// Toma un Docente y lo convierte en un XML
        /// </summary>
        /// <param name="docente">Docente a convertir</param>
        /// <returns>Node Cargado</returns>
        private XmlNode NuevoDocente(EDocente docente)
        {
            XmlNode xmlDc = xmlD.CreateElement("Docente");

            XmlElement xID = xmlD.CreateElement("Id_Curso");
            xID.InnerText = ProximoId().ToString();
            xmlDc.AppendChild(xID);

            XmlElement xPIN = xmlD.CreateElement("PIN");
            xPIN.InnerText = docente.Pin.ToString();
            xmlDc.AppendChild(xPIN);

            XmlElement xCED = xmlD.CreateElement("Cedula");
            xCED.InnerText = docente.Cedula;
            xmlDc.AppendChild(xCED);

            XmlElement xNom = xmlD.CreateElement("Nombre");
            xNom.InnerText = docente.Nombre;
            xmlDc.AppendChild(xNom);

            XmlElement xAu = xmlD.CreateElement("Apellido");
            xAu.InnerText = docente.Apellido;
            xmlDc.AppendChild(xAu);

            XmlElement xFI = xmlD.CreateElement("Email");
            xFI.InnerText = docente.Email;
            xmlDc.AppendChild(xFI);

            XmlElement xFF = xmlD.CreateElement("Telefono");
            xFF.InnerText = docente.Telefono;
            xmlDc.AppendChild(xFF);

            XmlElement xCupo = xmlD.CreateElement("Cantidad_Tardias");
            xCupo.InnerText = docente.CantidadT.ToString();
            xmlDc.AppendChild(xCupo);

            XmlElement xHo = xmlD.CreateElement("Cantidad_Ausencias");
            xHo.InnerText = docente.CantidadA.ToString();
            xmlDc.AppendChild(xHo);

            XmlElement xAc = xmlD.CreateElement("Id_Cursos");
            xAc.InnerText = IdCursos(docente.Cursos);
            xmlDc.AppendChild(xAc);

            XmlElement activo = xmlD.CreateElement("Activo");
            activo.InnerText = "1";
            xmlDc.AppendChild(activo);


            return xmlDc;
        }
        /// <summary>
        /// Busca un Docente por ID
        /// </summary>
        /// <param name="text">Id del docente</param>
        /// <returns></returns>
        public EDocente BuscarPorPin(string text)
        {
            xmlD.Load(miRutaXml);
            String id = String.Format("Docentes/Docente");
            XmlNodeList docentes = xmlD.SelectNodes(id);
            foreach (XmlNode node in docentes)
            {
                if (node.ChildNodes[1].InnerText.ToString() == text)
                {
                    return CargarDocente(node);
                }
            }
            return null;
        }
        /// <summary>
        /// Carga Todos los Docentes
        /// </summary>
        /// <returns>lista de Docentes</returns>
        public List<EDocente> DestacadoMenos()
        {
            List<EDocente> docentes = new List<EDocente>();
            EDocente mejor = buscarDestacado();
            EDocente peor = buscarMenosDestacado();
            docentes.Add(mejor);
            docentes.Add(peor);
            return docentes;
        }
        /// <summary>
        /// Agrega una Tardia a un Docente
        /// </summary>
        /// <param name="docente"></param>
        public void AgregarTardia(EDocente docente)
        {
            xmlD.Load(miRutaXml);
            String id = String.Format("Docentes/Docente");
            XmlNodeList docentes = xmlD.SelectNodes(id);
            foreach (XmlNode node in docentes)
            {
                if (node.FirstChild.InnerText.ToString() == docente.Id.ToString())
                {
                    int aux = Convert.ToInt32(node.ChildNodes[7].InnerText);
                    int sum = 1 + Convert.ToInt32(node.ChildNodes[7].InnerText);
                    node.ChildNodes[7].InnerText = sum.ToString();
                    xmlD.Save(miRutaXml);
                }
            }
        }
        /// <summary>
        /// Busca a un Docente Y lo Ingresa
        /// </summary>
        /// <param name="pin">Pin del docente</param>
        /// <returns>Un Docente</returns>
        public EDocente Login(string pin)
        {
            xmlD.Load(miRutaXml);
            String id = String.Format("Docentes/Docente");
            XmlNodeList docentes = xmlD.SelectNodes(id);
            foreach (XmlNode node in docentes)
            {
                if (node.ChildNodes[1].InnerText.ToString() == pin)
                {
                    return CargarDocente(node);
                }
            }
            return null;
        }
        /// <summary>
        /// "Elimina un docente, lo desactiva"
        /// </summary>
        /// <param name="docente">Docente a desactivar</param>
        /// <returns>True: Desactivo False: No se pudo desactivar</returns>
        public bool Remove(EDocente docente)
        {
            xmlD.Load(miRutaXml);
            String id = String.Format("Docentes/Docente");
            XmlNodeList docentes = xmlD.SelectNodes(id);
            foreach (XmlNode node in docentes)
            {
                if (node.FirstChild.InnerText == docente.Id.ToString())
                {
                    node.LastChild.InnerText = "0";
                    xmlD.Save(miRutaXml);
                    return true;
                }
            }
            return false;

        }
        /// <summary>
        /// Tiene Todas las Validaciones del Docente en un solo metodo
        /// </summary>
        /// <param name="docente">Docente</param>
        private void Validaciones(EDocente docente)
        {
            if (PinRepetido(docente.Pin))
            {
                throw new Exception("Ya Existe un Docente con este PIN");
            }
            if (CedulaRepetida(docente.Cedula))
            {
                throw new Exception("Ya Existe un Docente con el mismo numero de Cedula");
            }
            if (CorreoRepetido(docente.Email))
            {
                throw new Exception("Ya Existe un Docente con el mismo Correo");
            }
            if (TeleRepetido(docente.Telefono))
            {
                throw new Exception("Ya Existe un Docente con el mismo Telefono Celular");
            }
        }
        /// <summary>
        /// Valida si el Telefono esta repetido
        /// </summary>
        /// <param name="telefono">Telefono del Docente</param>
        /// <returns>True: Si False: No</returns>
        private bool TeleRepetido(string telefono)
        {
            xmlD.Load(miRutaXml);
            String id = String.Format("Docentes/Docente");
            XmlNodeList docentes = xmlD.SelectNodes(id);
            foreach (XmlNode item in docentes)
            {
                if (item.ChildNodes[6].InnerText.Equals(telefono))
                {
                    return true;
                }
            }

            return false;
        }
        /// <summary>
        /// Valida Si el Correo esta Repetido
        /// </summary>
        /// <param name="email">Correo del Docente</param>
        /// <returns>True: Si False: No</returns>
        private bool CorreoRepetido(string email)
        {
            xmlD.Load(miRutaXml);
            String id = String.Format("Docentes/Docente");
            XmlNodeList docentes = xmlD.SelectNodes(id);
            foreach (XmlNode item in docentes)
            {
                if (item.ChildNodes[5].InnerText.Equals(email))
                {
                    return true;
                }
            }

            return false;
        }
        /// <summary>
        /// Valida si la Cedula Esta Repetida
        /// </summary>
        /// <param name="ced">Cedula del Docente</param>
        /// <returns>True: Si False: No</returns>
        private bool CedulaRepetida(string ced)
        {
            xmlD.Load(miRutaXml);
            String id = String.Format("Docentes/Docente");
            XmlNodeList docentes = xmlD.SelectNodes(id);

            foreach (XmlNode item in docentes)
            {
                if (item.ChildNodes[2].InnerText.Equals(ced))
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Valida si el Pin del Docente Esta Repetido
        /// </summary>
        /// <param name="pin">Pin Del Docnete</param>
        /// <returns>True: Si False: No</returns>
        private bool PinRepetido(int pin)
        {
            xmlD.Load(miRutaXml);
            String id = String.Format("Docentes/Docente");
            XmlNodeList docentes = xmlD.SelectNodes(id);
            foreach (XmlNode node in docentes)
            {
                if (node.ChildNodes[1].InnerText.Equals(pin.ToString()))
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Busca el Proximo ID del docnete
        /// </summary>
        /// <returns></returns>
        private int ProximoId()
        {
            int id = 1;
            while (true)
            {
                if (BuscarPorID(id.ToString()).Equals(id.ToString()))
                {
                    id++;
                }
                else
                {
                    return id;
                }
            }
        }
        /// <summary>
        /// Busca el proximo Valor por el String indicado
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public string BuscarPorID(string num)
        {
            string siguiente = "";
            xmlD.Load(miRutaXml);
            String id = String.Format("Docentes/Docente");
            XmlNodeList docentes = xmlD.SelectNodes(id);

            foreach (XmlNode node in docentes)
            {
                if (node.FirstChild.InnerText.Equals(num))
                {
                    siguiente = node.FirstChild.InnerText;
                }
            }
            return siguiente;
        }
        /// <summary>
        /// Toma Los cursos del Docente los convierte en un String con los Id de los cursos
        /// </summary>
        /// <param name="cursos">Cursos del docente</param>
        /// <returns></returns>
        private string IdCursos(List<ECurso> cursos)
        {
            string numeros = "";
            foreach (ECurso item in cursos)
            {
                numeros += item.Id.ToString() + ";";
            }
            return numeros;
        }
        /// <summary>
        /// Carga todos los Docentes
        /// </summary>
        /// <returns>list Docentes</returns>
        public List<EDocente> CargarDocentes()
        {
            List<EDocente> docentes = new List<EDocente>();
            xmlD.Load(miRutaXml);
            String id = String.Format("Docentes/Docente");
            XmlNodeList listaDoc = xmlD.SelectNodes(id);
            foreach (XmlNode node in listaDoc)
            {
                if (node.LastChild.InnerText.Equals("1"))
                {
                    docentes.Add(CargarDocente(node));
                }
            }
            return docentes;
        }
        /// <summary>
        /// Toma un Nodo y lo converte en un docente
        /// </summary>
        /// <param name="node">Nodo a convertir </param>
        /// <returns>Docente Cargado</returns>
        private EDocente CargarDocente(XmlNode node)
        {
            EDocente doce = new EDocente
            {
                Id = Convert.ToInt32(node.FirstChild.InnerText),
                Pin = Convert.ToInt32(node.ChildNodes[1].InnerText),
                Cedula = node.ChildNodes[2].InnerText,
                Nombre = node.ChildNodes[3].InnerText,
                Apellido = node.ChildNodes[4].InnerText,
                Email = node.ChildNodes[5].InnerText,
                Telefono = node.ChildNodes[6].InnerText,
                CantidadT = Convert.ToInt32(node.ChildNodes[7].InnerText),
                CantidadA = Convert.ToInt32(node.ChildNodes[8].InnerText),
                Cursos = CargarCursos(node.ChildNodes[9].InnerText),
                Activo = Convert.ToInt32(node.LastChild.InnerText)
            };
            return doce;
        }
        /// <summary>
        /// Carga los Cursos, Toma el Strigs de iD y carga cada uno de los cursos
        /// </summary>
        /// <param name="ides">String con ids</param>
        /// <returns>Lista de Cursos</returns>
        private List<ECurso> CargarCursos(string ides)
        {
            List<ECurso> listaCursos = new List<ECurso>();
            string[] cursos = ides.Split(';');
            for (int i = 0; i < cursos.Length; i++)
            {
                if (!cursos[i].Equals(""))
                {
                    listaCursos.Add(cdal.BuscarId(cursos[i]));

                }
            }

            return listaCursos;
        }
        /// <summary>
        /// Edita un Docente
        /// </summary>
        /// <param name="docente">Docente a modificar</param>
        /// <returns></returns>
        public bool Editar(EDocente docente)
        {
            xmlD.Load(miRutaXml);
            String id = String.Format("Docentes/Docente");
            XmlNodeList cursos = xmlD.SelectNodes(id);
            foreach (XmlNode node in cursos)
            {
                if (node.FirstChild.InnerText.Equals(docente.Id.ToString()))
                {
                    node.ChildNodes[1].InnerText = docente.Pin.ToString();
                    node.ChildNodes[2].InnerText = docente.Cedula;
                    node.ChildNodes[3].InnerText = docente.Nombre;
                    node.ChildNodes[4].InnerText = docente.Apellido;
                    node.ChildNodes[5].InnerText = docente.Email;
                    node.ChildNodes[6].InnerText = docente.Telefono;
                    node.ChildNodes[7].InnerText = docente.CantidadT.ToString();
                    node.ChildNodes[8].InnerText = docente.CantidadA.ToString();
                    node.ChildNodes[9].InnerText = IdCursos(docente.Cursos);
                    node.LastChild.InnerText = docente.Activo.ToString();

                    xmlD.Save(miRutaXml);
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Busca al docente mesno Destacado
        /// </summary>
        /// <returns>Docente </returns>
        private EDocente buscarMenosDestacado()
        {
            xmlD.Load(miRutaXml);
            String id = String.Format("Docentes/Docente");
            XmlNodeList listaDoc = xmlD.SelectNodes(id);
            foreach (XmlNode node in listaDoc)
            {
                if (node.ChildNodes[8].InnerText=="0")
                {
                    return CargarDocente(node);
                }
            }
            return null;
        }
        /// <summary>
        /// Busca al docente mas Destacado
        /// </summary>
        /// <returns>Docente </returns>
        private EDocente buscarDestacado()
        {
            xmlD.Load(miRutaXml);
            String id = String.Format("Docentes/Docente");
            XmlNodeList listaDoc = xmlD.SelectNodes(id);
            foreach (XmlNode node in listaDoc)
            {
                if (Convert.ToInt32(node.ChildNodes[8].InnerText) > 0)
                {
                    return CargarDocente(node);
                }
            }
            return null;
        }
    }
}
