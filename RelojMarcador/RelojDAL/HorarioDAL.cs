﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RelojMarcadorENL;
using System.Xml.Linq;
using System.Globalization;
using System.Xml;

namespace RelojMarcadorDAL
{
    public class HorarioDAL
    {
        private string miRutaXml;
        private XmlDocument xmlH;

        public HorarioDAL()
        {
            xmlH = new XmlDocument();
            miRutaXml = @"C:\Users\Fabricio\Documents\Progra III\Ptoyecto3.0\xml\Horarios.xml";
        }
        /// <summary>
        /// Crea un Nuevo horario y lo ingresa Al XML
        /// </summary>
        /// <param name="horario">EHorario:  Horario a Ingresar </param>
        /// <returns>Bool:True: Lo Ingreso False: No lo ingreso.</returns>
        public bool Agregar(EHorario horario)
        {
            if (EstaRepetido(horario))
            {
                throw new Exception("Ya existe un Horario con"+"\n"+"los mismos Valores");
            }
            else
            {
                xmlH.Load(miRutaXml);

                XmlNode xCurso = NuevoHorario(horario);

                XmlNode nodoRaiz = xmlH.DocumentElement;

                nodoRaiz.InsertAfter(xCurso, nodoRaiz.LastChild);

                xmlH.Save(miRutaXml);

                return true;

            }
        }
        /// <summary>
        /// Toma un Nodo y Lo "Elimina" le pone el valor activo en Falso
        /// </summary>
        /// <param name="horario">EHorario A Desactivar</param>
        /// <returns>True: Lo Ingreso False: No lo ingreso</returns>
        public bool Remove(EHorario horario)
        {
            xmlH.Load(miRutaXml);
            String id = String.Format("Horarios/Horario");
            XmlNodeList docentes = xmlH.SelectNodes(id);
            foreach (XmlNode node in docentes)
            {
                if (node.FirstChild.InnerText == horario.Id.ToString())
                {
                    node.LastChild.InnerText = "0";
                    xmlH.Save(miRutaXml);
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Toma Un Ehorario y lo Convierte en un Nodo.
        /// </summary>
        /// <param name="horario">EHorario a Convertir</param>
        /// <returns>Xml Node: El horario hecho un Node</returns>
        private XmlNode NuevoHorario(EHorario horario)
        {
            XmlNode xhorario = xmlH.CreateElement("Horario");

            XmlElement xId = xmlH.CreateElement("Id_Horario");
            xId.InnerText = ProximoId().ToString();
            xhorario.AppendChild(xId);

            XmlElement xDia = xmlH.CreateElement("Dia");
            xDia.InnerText = horario.Dia;
            xhorario.AppendChild(xDia);

            XmlElement xHI = xmlH.CreateElement("Hora_Inicial");
            xHI.InnerText = horario.HoraInicio;
            xhorario.AppendChild(xHI);

            XmlElement xFI = xmlH.CreateElement("Hora_Final");
            xFI.InnerText = horario.HoraFinal;
            xhorario.AppendChild(xFI);

            XmlElement xAc = xmlH.CreateElement("Activo");
            xAc.InnerText = "1";
            xhorario.AppendChild(xAc);
            

            return xhorario;
        }
        /// <summary>
        /// Toma un String En Numero para Buscar el Siguiente numero
        /// </summary>
        /// <param name="num">String: Con el Valor </param>
        /// <returns>String con el Valor</returns>
        private string BuscarPorID(string num)
        {
            string siguiente = "";
            xmlH.Load(miRutaXml);
            String id = String.Format("Horarios/Horario");
            XmlNodeList docentes = xmlH.SelectNodes(id);

            foreach (XmlNode node in docentes)
            {
                if (node.FirstChild.InnerText.Equals(num))
                {
                    siguiente = node.FirstChild.InnerText;
                }
            }
            return siguiente;
        }
        /// <summary>
        /// Calcula el Proximo Id en la linea
        /// </summary>
        /// <returns>Int32: Con el Id siguiente </returns>
        private int ProximoId()
        {
            int id = 1;
            while (true)
            {
                if (BuscarPorID(id.ToString()).Equals(id.ToString()))
                {
                    id++;
                }
                else
                {
                    return id;
                }
            }
        }
       /// <summary>
       /// Carga Todos los Horarios Activosd del Xml
       /// </summary>
       /// <returns>List Con Los Horarios </returns>
        public List<EHorario> CargarHorarios()
        {
            List<EHorario> horarios = new List<EHorario>();
            xmlH.Load(miRutaXml);
            String id = String.Format("Horarios/Horario");
            XmlNodeList listaDoc = xmlH.SelectNodes(id);
            foreach (XmlNode node in listaDoc)
            {
                if (node.LastChild.InnerText.Equals("1"))
                {
                    horarios.Add(CargarHorario(node));
                }
            }

            return horarios;
        }
        /// <summary>
        /// Toma un Nodo XML y lo hace un Horario
        /// </summary>
        /// <param name="node">Nodo A convertir </param>
        /// <returns>Un Ehorario Cargado</returns>
        public EHorario CargarHorario(XmlNode node)
        {
            EHorario e = new EHorario
            {
                Id = Convert.ToInt32(node.FirstChild.InnerText),
                Dia = node.ChildNodes[1].InnerText,
                HoraInicio = node.ChildNodes[2].InnerText,
                HoraFinal = node.ChildNodes[3].InnerText,
                Activo = Convert.ToInt32(node.LastChild.InnerText)
            };
            return e;
        }
        /// <summary>
        /// Indica si el Horario que se Quiere Ingresar Esta Repetido
        /// </summary>
        /// <param name="oldHorario"></param>
        /// <returns></returns>
        public bool EstaRepetido(EHorario oldHorario)
        {
            xmlH.Load(miRutaXml);
            String id = String.Format("Horarios/Horario");
            XmlNodeList horarios = xmlH.SelectNodes(id);

            foreach (XmlNode node in horarios)
            {
                if (node.ChildNodes[1].InnerText.Equals(oldHorario.Dia) &&
                    node.ChildNodes[2].InnerText.Equals(oldHorario.HoraInicio) &&
                    node.ChildNodes[3].InnerText.Equals(oldHorario.HoraFinal))
                {
                    return true;
                }
            }
            return false;
        }
       /// <summary>
       /// Busca Un horario con UN Respectivo Id y lo devuelve
       /// </summary>
       /// <param name="idH">String Id del Horario </param>
       /// <returns>Horario: Horario dueño del ID</returns>
        public EHorario CargarHorarioPorId(string idH)
        {

            xmlH.Load(miRutaXml);
            String id = String.Format("Horarios/Horario");
            XmlNodeList docentes = xmlH.SelectNodes(id);
            foreach (XmlNode node in docentes)
            {
                if (node.FirstChild.InnerText.Equals(idH))
                {
                    return CargarHorario(node);
                }
            }
            return null;
        }
        /// <summary>
        /// Edita un Horario y lo Remplaza en el XML
        /// </summary>
        /// <param name="hora">EHorario a Editar</param>
        /// <returns></returns>
        public bool Editar(EHorario hora)
        {
            xmlH.Load(miRutaXml);
            String id = String.Format("Horarios/Horario");
            XmlNodeList cursos = xmlH.SelectNodes(id);
            foreach (XmlNode node in cursos)
            {
                if (node.FirstChild.InnerText == hora.Id.ToString())
                {
                    node.ChildNodes[1].InnerText = hora.Dia;
                    node.ChildNodes[2].InnerText = hora.HoraInicio;
                    node.ChildNodes[3].InnerText = hora.HoraFinal;
                    node.LastChild.InnerText = hora.Activo.ToString();

                    xmlH.Save(miRutaXml);
                    return true;
                }
            }

            return false;
        }

    }
}
