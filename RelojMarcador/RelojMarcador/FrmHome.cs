﻿using RelojMarcadorBOL;
using RelojMarcadorENL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RelojMarcador
{
    public partial class FrmHome : Form
    {
        private DocenteBOL dbol;
        private MarcaBOL mbol;
        private EDocente docente;
        private EMarca marca;
        private ECurso cursoOnDuty;
        private string introduccioDocente;
        private string justificacionDocente;

        public FrmHome()
        {
            InitializeComponent();
            CenterToScreen();
            timer1.Enabled = true;
        }

        private void Home_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            dbol = new DocenteBOL();
            docente = new EDocente();
            cursoOnDuty = new ECurso();
            marca = new EMarca();
            mbol = new MarcaBOL();
            txtmPIN.ReadOnly = true;
            introduccioDocente = "";
            justificacionDocente = "";
            Colores();
        }

        private void btnFLogin_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 2;
        }

        private void btnFConsulta_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 1;
        }

        private void btnFMant_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToString("HH:mm:ss");
            lblDate.Text = DateTime.Now.ToString("dd/mm/yyyy");
        }

        private void btnAgregarH_Click_1(object sender, EventArgs e)
        {
            FrmEditar frm = new FrmEditar();
            frm.Show(this);
            Hide();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 2;
        }

        private void btnConsultas_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 1;

        }

        private void btnMant_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            FrmEditar frm = new FrmEditar();
            frm.Show(this);
            Hide();
        }

        private void btnUno_Click(object sender, EventArgs e)
        {
            try
            {
                txtmPIN.Text += "1";
            }
            catch (Exception)
            {

                lblErrorL.Text = "Intente Nuevamente";
            }
        }

        private void btnDos_Click(object sender, EventArgs e)
        {
            try
            {
                txtmPIN.Text += "2";
            }
            catch (Exception)
            {

                lblErrorL.Text = "Intente Nuevamente";
            }
        }

        private void btnTres_Click(object sender, EventArgs e)
        {
            try
            {
                txtmPIN.Text += "3";
            }
            catch (Exception)
            {

                lblErrorL.Text = "Intente Nuevamente";
            }
        }

        private void btnCuatro_Click(object sender, EventArgs e)
        {
            try
            {
                txtmPIN.Text += "4";
            }
            catch (Exception)
            {

                lblErrorL.Text = "Intente Nuevamente";
            }
        }

        private void btnCinco_Click(object sender, EventArgs e)
        {
            try
            {
                txtmPIN.Text += "5";
            }
            catch (Exception)
            {

                lblErrorL.Text = "Intente Nuevamente";
            }
        }

        private void btnSeis_Click(object sender, EventArgs e)
        {
            try
            {
                txtmPIN.Text += "6";
            }
            catch (Exception)
            {

                lblErrorL.Text = "Intente Nuevamente";
            }
        }

        private void btnSiete_Click(object sender, EventArgs e)
        {
            try
            {
                txtmPIN.Text += "7";
            }
            catch (Exception)
            {

                lblErrorL.Text = "Intente Nuevamente";
            }
        }

        private void btnOcho_Click(object sender, EventArgs e)
        {
            try
            {
                txtmPIN.Text += "8";
            }
            catch (Exception)
            {

                lblErrorL.Text = "Intente Nuevamente";
            }
        }

        private void btnNueve_Click(object sender, EventArgs e)
        {
            try
            {
                txtmPIN.Text += "9";
            }
            catch (Exception)
            {

                lblErrorL.Text = "Intente Nuevamente";
            }
        }

        private void btnCero_Click(object sender, EventArgs e)
        {
            try
            {
                txtmPIN.Text += "0";
            }
            catch (Exception)
            {

                lblErrorL.Text = "Intente Nuevamente";
            }
        }

        private void btnErase_Click(object sender, EventArgs e)
        {
            try
            {
                txtmPIN.Text = "";
            }
            catch (Exception)
            {

                throw;
            }

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                int hasta = txtmPIN.Text.Length - 1;
                string resultado = txtmPIN.Text.Substring(0, hasta);
                txtmPIN.Text = resultado;
            }
            catch (Exception)
            {
                lblErrorL.Text = "Por Favor ingrese Valores.";
            }
        }

        private void btnLog_Click(object sender, EventArgs e)
        {

            docente = dbol.Login(txtmPIN.Text.Trim());

            if (docente == null)
            {
                throw new Exception("No se Encontro el Docente");
            }
            else
            {
                marca = mbol.MarcaCreada(docente); //Tiene Marca Creada y Conicede EL Dia hoy?
                TieneCurso(docente);

                if (marca != null) //Saliendo //Si  && Tiene Entrada Y No tiene Salida ?
                {
                    if (SalioAntes())
                    {
                        if (MessageBox.Show("Se Esta Retirando Antes de Finalizar el Curso.",
                            "Desea Salir de todos modos ?:",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            DoSalida();
                        }
                        else
                        {
                            tabControl1.SelectedIndex = 3;
                            txtmPIN.Text = "";
                        }
                    }
                    else
                    {
                        tabControl1.SelectedIndex = 4;
                    }
                    //Sale Despues
                }
                else
                {
                    if (TieneCurso(docente))
                    {
                        if (LLegoAntes()) //Si LLega Antes Metodo!
                        {
                            btnEntr1.Text = "Curso";
                            btnEntr2.Text = "Consulta/Curso";
                            btnEntr3.Text = "Reunión/Curso";
                            tabControl1.SelectedIndex = 3;
                        }
                        else
                        {
                            if (LLegoTarde())
                            {
                                introduccioDocente = "LLego Tarde";
                                DoEntMarca();
                                MessageBox.Show("Se te acaba de asignar una Tardia.", "Question:",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                dbol.AgregarTardia(docente);
                            }
                            else
                            {
                                DoEntMarca();
                            }
                        }

                    }
                    else
                    {
                        btnEntr1.Text = "Consulta";
                        btnEntr2.Text = "Reunión";
                        btnEntr3.Text = "Otro";
                        tabControl1.SelectedIndex = 3;
                    }

                }
            }

        }
        /// <summary>
        /// Inidica si el Docente Esta LLegando Tarde
        /// </summary>
        /// <returns>True: Llego Tarde False: LLego Temprano</returns>
        private bool LLegoTarde()
        {
            string[] horas = cursoOnDuty.Horario.HoraInicio.Split(':');
            int hora = Convert.ToInt32(DateTime.Now.ToString("HH"));
            int min = Convert.ToInt32(DateTime.Now.ToString("mm"));

            if (hora >= Convert.ToInt32(horas[0]))
            {
                if (min > Convert.ToInt32(horas[1]))
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Inidica si el Docente Esta LLegando Antes de La hora de su curso
        /// </summary>
        /// <returns>True: Llego Antes False:No llego</returns>
        private bool LLegoAntes()
        {
            string[] horas = cursoOnDuty.Horario.HoraInicio.Split(':');
            int hora = Convert.ToInt32(DateTime.Now.ToString("HH"));
            int min = Convert.ToInt32(DateTime.Now.ToString("mm"));
            if (hora <= Convert.ToInt32(horas[0]))
            {
                if (min < Convert.ToInt32(horas[1]))
                {
                    return true;
                }
            }
            return false;
        }
        // <summary>
        /// Inidica si el Docente Esta Saliendo Antes de La hora de su curso
        /// </summary>
        /// <returns>True: Llego Antes False:No llego</returns>
        private bool SalioAntes()
        {
            string[] horas = cursoOnDuty.Horario.HoraFinal.Split(':');
            int hora = Convert.ToInt32(DateTime.Now.ToString("HH"));
            int min = Convert.ToInt32(DateTime.Now.ToString("mm"));
            if (hora < Convert.ToInt32(horas[0]))
            {
                return true;
            }
            else if (hora == Convert.ToInt32(horas[0]))
            {
                if (min < Convert.ToInt32(horas[1]))
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Limpia el txt Error;
        /// </summary>
        private void limpiar()
        {
            lblErrorL.Text = "Mensaje";
        }
        /// <summary>
        /// Indica si El docente tiene un Curso Que Impartir hoy
        /// </summary>
        /// <param name="docente">Docente A Evalaur</param>
        /// <returns>True: SI False: NO</returns>
        private bool TieneCurso(EDocente docente)
        {
            foreach (ECurso curso in docente.Cursos)
            {
                if (CoincideDia(curso.Horario.Dia))
                {
                    cursoOnDuty = curso;
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Indica si coinciden los Dias 
        /// </summary>
        /// <param name="dia">String con la fecha</param>
        /// <returns>True: SI False: NO</returns>
        private bool CoincideDia(string dia)
        {
            string date = new CultureInfo("en-US").TextInfo.ToTitleCase(DateTime.Now.ToString("dddd"));
            string[] dias = dia.Split('/');
            foreach (string xdia in dias)
            {
                if (xdia.Equals(date))
                {
                    return true;

                }
            }
            return false;
        }

        private void btnEntr1_Click(object sender, EventArgs e)
        {
            try
            {
                introduccioDocente = btnEntr1.Text;
                DoEntMarca();
            }
            catch (Exception)
            {

                lblErrorL.Text = "Problemas con el mensaje de Entrada";
            }
        }

        private void btnEntr2_Click(object sender, EventArgs e)
        {
            try
            {
                introduccioDocente = btnEntr2.Text;
                DoEntMarca();
            }
            catch (Exception)
            {

                lblErrorL.Text = "Problemas con el mensaje de Entrada";

            }
        }

        private void btnEntr3_Click(object sender, EventArgs e)
        {
            try
            {
                introduccioDocente = btnEntr3.Text;
                DoEntMarca();

            }
            catch (Exception)
            {
                lblErrorL.Text = "Problemas con el mensaje de Entrada";
            }
        }

        private void btnSal1_Click(object sender, EventArgs e)
        {
            try
            {
                justificacionDocente = btnSal1.Text;
                DoSalida();
            }
            catch (Exception)
            {

                lblErrorL.Text = "Problemas con el mensaje de Salida";

            }

        }

        private void btnSal2_Click(object sender, EventArgs e)
        {
            try
            {
                justificacionDocente = btnSal2.Text;
                DoSalida();
            }
            catch (Exception)
            {

                lblErrorL.Text = "Problemas con el mensaje de Salida";

            }
        }

        private void btnSal3_Click(object sender, EventArgs e)
        {
            try
            {
                justificacionDocente = btnSal3.Text;
                DoSalida();

            }
            catch (Exception)
            {

                lblErrorL.Text = "Problemas con el mensaje de Salida";

            }
        }
        /// <summary>
        /// Realiza la entrada del DOCENTE
        /// </summary>
        private void DoEntMarca()
        {
            mbol.NuevoIngreso(docente, introduccioDocente);  //&& Cree Marca Y Asigne Entrada
            tabControl1.SelectedIndex = 2;
            txtmPIN.Text = "";
        }
        /// <summary>
        /// Realiza la salida del DOCENTE
        /// </summary>
        private void DoSalida()
        {
            marca.Justificacion = justificacionDocente;
            mbol.HacerSalida(marca);
            tabControl1.SelectedIndex = 2;
            txtmPIN.Text = "";

        }

        private void btnAuTa_Click(object sender, EventArgs e)
        {
            try
            {
                List<EDocente> docente = new List<EDocente>();
                docente.Add(dbol.BuscarPorPin(txtBuscar.Text.Trim()));
                dgvConsultas.DataSource = docente;
            }
            catch (Exception)
            {
                lblErrorCon.Text = "No se Encontro Docente con Ese Pin";
            }

        }

        private void btnDestacados_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtBuscar.Text.Trim().Equals(""))
                {
                    dgvDestacados.DataSource = dbol.CargaTodos();
                }
                else
                {
                    List<EDocente> docente = new List<EDocente>();
                    docente = dbol.DestcadoMenos();
                    dgvDestacados.DataSource = docente;
                }

            }
            catch (Exception)
            {

                lblErrorCon.Text = "No se puedo Cargar Los Docentes";

            }
        }

        private void btnMarcas_Click(object sender, EventArgs e)
        {
            try
            {
                List<EMarca> marcas = mbol.BuscarPor(txtBuscar.Text.Trim());
                dgvMarcas.DataSource = marcas;
            }
            catch (Exception ez)
            {

                lblErrorCon.Text = "No Se Encontraron marcas de Ese Doncente";

            }
        }
        /// <summary>
        /// Asigna los colores a los DaraGridView
        /// </summary>
        private void Colores()
        {
            dgvMarcas.ForeColor = Color.Black;
            dgvDestacados.ForeColor = Color.Black;
            dgvConsultas.ForeColor = Color.Black;
        }
    }
}
