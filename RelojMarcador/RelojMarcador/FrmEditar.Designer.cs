﻿namespace RelojMarcador
{
    partial class FrmEditar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCargarC = new System.Windows.Forms.Button();
            this.btnCargarD = new System.Windows.Forms.Button();
            this.btnCargarH = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.aTabDocente = new System.Windows.Forms.TabPage();
            this.btnNuevoD = new System.Windows.Forms.Button();
            this.gpDocente = new System.Windows.Forms.GroupBox();
            this.lblErrorD = new System.Windows.Forms.Label();
            this.btnAgregarDocente = new System.Windows.Forms.Button();
            this.btnCargarDgvCD = new System.Windows.Forms.Button();
            this.dgvCursosDocente = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aulaDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaInicioDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaFinalDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cupoDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.horarioDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activoDataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eCursoBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.cbCursosDisponibles = new System.Windows.Forms.ComboBox();
            this.eCursoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtmTeleD = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtEmailD = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtxApellidoD = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNombreD = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtmCedD = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtmPinD = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnElimiDgvD = new System.Windows.Forms.Button();
            this.btnCargaDgvD = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvDocentes = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pinDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cedulaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apellidoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emailDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.telefonoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantidadTDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantidadADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activoDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eDocenteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aTabHorario = new System.Windows.Forms.TabPage();
            this.btnNuevoH = new System.Windows.Forms.Button();
            this.dgvHorarios = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.horaInicioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.horaFinalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eHorarioBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.gpHorario = new System.Windows.Forms.GroupBox();
            this.btnAgregarHorario = new System.Windows.Forms.Button();
            this.lblErrorH = new System.Windows.Forms.Label();
            this.cbHoraFinal = new System.Windows.Forms.ComboBox();
            this.cbHoraInicial = new System.Windows.Forms.ComboBox();
            this.cbDiaHorario = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.btnElimiDgvH = new System.Windows.Forms.Button();
            this.btnCargaDgvH = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.aTabCurso = new System.Windows.Forms.TabPage();
            this.btnNuevoC = new System.Windows.Forms.Button();
            this.gpCurso = new System.Windows.Forms.GroupBox();
            this.txtmCupo = new System.Windows.Forms.MaskedTextBox();
            this.txtmAula = new System.Windows.Forms.MaskedTextBox();
            this.btnAgregarCurso = new System.Windows.Forms.Button();
            this.lblErrorC = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.cbHorariosC = new System.Windows.Forms.ComboBox();
            this.eHorarioBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.dtpFinal = new System.Windows.Forms.DateTimePicker();
            this.dtpInicio = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.txtNombreC = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.btnElimiDgvC = new System.Windows.Forms.Button();
            this.btnCargaDgvC = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.dgvCursos = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aulaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaInicioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaFinalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cupoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.horarioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activoDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eCursoBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.aTabDocente.SuspendLayout();
            this.gpDocente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCursosDocente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eCursoBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eCursoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocentes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eDocenteBindingSource)).BeginInit();
            this.aTabHorario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHorarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eHorarioBindingSource1)).BeginInit();
            this.gpHorario.SuspendLayout();
            this.aTabCurso.SuspendLayout();
            this.gpCurso.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eHorarioBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCursos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eCursoBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(113)))), ((int)(((byte)(149)))));
            this.panel1.Controls.Add(this.btnCargarC);
            this.panel1.Controls.Add(this.btnCargarD);
            this.panel1.Controls.Add(this.btnCargarH);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(844, 40);
            this.panel1.TabIndex = 0;
            // 
            // btnCargarC
            // 
            this.btnCargarC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(141)))), ((int)(((byte)(178)))));
            this.btnCargarC.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(141)))), ((int)(((byte)(178)))));
            this.btnCargarC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCargarC.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCargarC.ForeColor = System.Drawing.Color.White;
            this.btnCargarC.Location = new System.Drawing.Point(198, 8);
            this.btnCargarC.Name = "btnCargarC";
            this.btnCargarC.Size = new System.Drawing.Size(74, 38);
            this.btnCargarC.TabIndex = 6;
            this.btnCargarC.Text = "Curso";
            this.btnCargarC.UseVisualStyleBackColor = false;
            this.btnCargarC.Click += new System.EventHandler(this.btnCargarC_Click);
            // 
            // btnCargarD
            // 
            this.btnCargarD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(141)))), ((int)(((byte)(178)))));
            this.btnCargarD.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(141)))), ((int)(((byte)(178)))));
            this.btnCargarD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCargarD.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCargarD.ForeColor = System.Drawing.Color.White;
            this.btnCargarD.Location = new System.Drawing.Point(21, 8);
            this.btnCargarD.Name = "btnCargarD";
            this.btnCargarD.Size = new System.Drawing.Size(90, 38);
            this.btnCargarD.TabIndex = 7;
            this.btnCargarD.Text = "Docente";
            this.btnCargarD.UseVisualStyleBackColor = false;
            this.btnCargarD.Click += new System.EventHandler(this.btnCargarD_Click);
            // 
            // btnCargarH
            // 
            this.btnCargarH.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(141)))), ((int)(((byte)(178)))));
            this.btnCargarH.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(141)))), ((int)(((byte)(178)))));
            this.btnCargarH.FlatAppearance.BorderSize = 0;
            this.btnCargarH.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCargarH.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCargarH.ForeColor = System.Drawing.Color.White;
            this.btnCargarH.Location = new System.Drawing.Point(117, 8);
            this.btnCargarH.Name = "btnCargarH";
            this.btnCargarH.Size = new System.Drawing.Size(75, 38);
            this.btnCargarH.TabIndex = 8;
            this.btnCargarH.Text = "Horario";
            this.btnCargarH.UseVisualStyleBackColor = false;
            this.btnCargarH.Click += new System.EventHandler(this.btnCargarH_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(141)))), ((int)(((byte)(178)))));
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(823, 40);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(21, 471);
            this.panel3.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(141)))), ((int)(((byte)(178)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 40);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(21, 471);
            this.panel2.TabIndex = 3;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(113)))), ((int)(((byte)(149)))));
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(21, 483);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(802, 28);
            this.panel4.TabIndex = 4;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.aTabDocente);
            this.tabControl1.Controls.Add(this.aTabHorario);
            this.tabControl1.Controls.Add(this.aTabCurso);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.ItemSize = new System.Drawing.Size(0, 1);
            this.tabControl1.Location = new System.Drawing.Point(21, 40);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(802, 443);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 0;
            // 
            // aTabDocente
            // 
            this.aTabDocente.BackColor = System.Drawing.Color.Silver;
            this.aTabDocente.Controls.Add(this.btnNuevoD);
            this.aTabDocente.Controls.Add(this.gpDocente);
            this.aTabDocente.Controls.Add(this.btnElimiDgvD);
            this.aTabDocente.Controls.Add(this.btnCargaDgvD);
            this.aTabDocente.Controls.Add(this.label1);
            this.aTabDocente.Controls.Add(this.dgvDocentes);
            this.aTabDocente.Location = new System.Drawing.Point(4, 5);
            this.aTabDocente.Name = "aTabDocente";
            this.aTabDocente.Padding = new System.Windows.Forms.Padding(3);
            this.aTabDocente.Size = new System.Drawing.Size(794, 434);
            this.aTabDocente.TabIndex = 0;
            // 
            // btnNuevoD
            // 
            this.btnNuevoD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevoD.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevoD.ForeColor = System.Drawing.Color.White;
            this.btnNuevoD.Location = new System.Drawing.Point(479, 14);
            this.btnNuevoD.Name = "btnNuevoD";
            this.btnNuevoD.Size = new System.Drawing.Size(99, 30);
            this.btnNuevoD.TabIndex = 5;
            this.btnNuevoD.Text = "Nuevo";
            this.btnNuevoD.UseVisualStyleBackColor = true;
            this.btnNuevoD.Click += new System.EventHandler(this.btnNuevoD_Click);
            // 
            // gpDocente
            // 
            this.gpDocente.Controls.Add(this.lblErrorD);
            this.gpDocente.Controls.Add(this.btnAgregarDocente);
            this.gpDocente.Controls.Add(this.btnCargarDgvCD);
            this.gpDocente.Controls.Add(this.dgvCursosDocente);
            this.gpDocente.Controls.Add(this.label8);
            this.gpDocente.Controls.Add(this.cbCursosDisponibles);
            this.gpDocente.Controls.Add(this.txtmTeleD);
            this.gpDocente.Controls.Add(this.label7);
            this.gpDocente.Controls.Add(this.txtEmailD);
            this.gpDocente.Controls.Add(this.label6);
            this.gpDocente.Controls.Add(this.txtxApellidoD);
            this.gpDocente.Controls.Add(this.label5);
            this.gpDocente.Controls.Add(this.txtNombreD);
            this.gpDocente.Controls.Add(this.label4);
            this.gpDocente.Controls.Add(this.txtmCedD);
            this.gpDocente.Controls.Add(this.label3);
            this.gpDocente.Controls.Add(this.txtmPinD);
            this.gpDocente.Controls.Add(this.label2);
            this.gpDocente.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpDocente.ForeColor = System.Drawing.Color.White;
            this.gpDocente.Location = new System.Drawing.Point(10, 172);
            this.gpDocente.Name = "gpDocente";
            this.gpDocente.Size = new System.Drawing.Size(778, 256);
            this.gpDocente.TabIndex = 4;
            this.gpDocente.TabStop = false;
            this.gpDocente.Text = "Docente";
            // 
            // lblErrorD
            // 
            this.lblErrorD.AutoSize = true;
            this.lblErrorD.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorD.Location = new System.Drawing.Point(366, 222);
            this.lblErrorD.Name = "lblErrorD";
            this.lblErrorD.Size = new System.Drawing.Size(60, 17);
            this.lblErrorD.TabIndex = 61;
            this.lblErrorD.Text = "Mensaje";
            // 
            // btnAgregarDocente
            // 
            this.btnAgregarDocente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarDocente.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregarDocente.ForeColor = System.Drawing.Color.White;
            this.btnAgregarDocente.Location = new System.Drawing.Point(180, 213);
            this.btnAgregarDocente.Name = "btnAgregarDocente";
            this.btnAgregarDocente.Size = new System.Drawing.Size(180, 30);
            this.btnAgregarDocente.TabIndex = 5;
            this.btnAgregarDocente.Text = "Agregar";
            this.btnAgregarDocente.UseVisualStyleBackColor = true;
            this.btnAgregarDocente.Click += new System.EventHandler(this.btnAgregarDocente_Click);
            // 
            // btnCargarDgvCD
            // 
            this.btnCargarDgvCD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCargarDgvCD.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCargarDgvCD.ForeColor = System.Drawing.Color.White;
            this.btnCargarDgvCD.Location = new System.Drawing.Point(673, 20);
            this.btnCargarDgvCD.Name = "btnCargarDgvCD";
            this.btnCargarDgvCD.Size = new System.Drawing.Size(99, 30);
            this.btnCargarDgvCD.TabIndex = 5;
            this.btnCargarDgvCD.Text = "Cargar";
            this.btnCargarDgvCD.UseVisualStyleBackColor = true;
            this.btnCargarDgvCD.Click += new System.EventHandler(this.btnCargarDgvCD_Click);
            // 
            // dgvCursosDocente
            // 
            this.dgvCursosDocente.AllowUserToAddRows = false;
            this.dgvCursosDocente.AutoGenerateColumns = false;
            this.dgvCursosDocente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCursosDocente.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn3,
            this.nombreDataGridViewTextBoxColumn3,
            this.aulaDataGridViewTextBoxColumn2,
            this.fechaInicioDataGridViewTextBoxColumn2,
            this.fechaFinalDataGridViewTextBoxColumn2,
            this.cupoDataGridViewTextBoxColumn2,
            this.horarioDataGridViewTextBoxColumn2,
            this.activoDataGridViewTextBoxColumn4});
            this.dgvCursosDocente.DataSource = this.eCursoBindingSource2;
            this.dgvCursosDocente.Location = new System.Drawing.Point(366, 91);
            this.dgvCursosDocente.Name = "dgvCursosDocente";
            this.dgvCursosDocente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCursosDocente.Size = new System.Drawing.Size(406, 125);
            this.dgvCursosDocente.TabIndex = 60;
            this.dgvCursosDocente.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCursosDocente_CellContentClick);
            // 
            // idDataGridViewTextBoxColumn3
            // 
            this.idDataGridViewTextBoxColumn3.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn3.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn3.Name = "idDataGridViewTextBoxColumn3";
            this.idDataGridViewTextBoxColumn3.Visible = false;
            // 
            // nombreDataGridViewTextBoxColumn3
            // 
            this.nombreDataGridViewTextBoxColumn3.DataPropertyName = "Nombre";
            this.nombreDataGridViewTextBoxColumn3.HeaderText = "Nombre";
            this.nombreDataGridViewTextBoxColumn3.Name = "nombreDataGridViewTextBoxColumn3";
            // 
            // aulaDataGridViewTextBoxColumn2
            // 
            this.aulaDataGridViewTextBoxColumn2.DataPropertyName = "Aula";
            this.aulaDataGridViewTextBoxColumn2.HeaderText = "Aula";
            this.aulaDataGridViewTextBoxColumn2.Name = "aulaDataGridViewTextBoxColumn2";
            // 
            // fechaInicioDataGridViewTextBoxColumn2
            // 
            this.fechaInicioDataGridViewTextBoxColumn2.DataPropertyName = "FechaInicio";
            this.fechaInicioDataGridViewTextBoxColumn2.HeaderText = "FechaInicio";
            this.fechaInicioDataGridViewTextBoxColumn2.Name = "fechaInicioDataGridViewTextBoxColumn2";
            // 
            // fechaFinalDataGridViewTextBoxColumn2
            // 
            this.fechaFinalDataGridViewTextBoxColumn2.DataPropertyName = "FechaFinal";
            this.fechaFinalDataGridViewTextBoxColumn2.HeaderText = "FechaFinal";
            this.fechaFinalDataGridViewTextBoxColumn2.Name = "fechaFinalDataGridViewTextBoxColumn2";
            // 
            // cupoDataGridViewTextBoxColumn2
            // 
            this.cupoDataGridViewTextBoxColumn2.DataPropertyName = "Cupo";
            this.cupoDataGridViewTextBoxColumn2.HeaderText = "Cupo";
            this.cupoDataGridViewTextBoxColumn2.Name = "cupoDataGridViewTextBoxColumn2";
            // 
            // horarioDataGridViewTextBoxColumn2
            // 
            this.horarioDataGridViewTextBoxColumn2.DataPropertyName = "Horario";
            this.horarioDataGridViewTextBoxColumn2.HeaderText = "Horario";
            this.horarioDataGridViewTextBoxColumn2.Name = "horarioDataGridViewTextBoxColumn2";
            // 
            // activoDataGridViewTextBoxColumn4
            // 
            this.activoDataGridViewTextBoxColumn4.DataPropertyName = "Activo";
            this.activoDataGridViewTextBoxColumn4.HeaderText = "Activo";
            this.activoDataGridViewTextBoxColumn4.Name = "activoDataGridViewTextBoxColumn4";
            this.activoDataGridViewTextBoxColumn4.Visible = false;
            // 
            // eCursoBindingSource2
            // 
            this.eCursoBindingSource2.DataSource = typeof(RelojMarcadorENL.ECurso);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(176, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(153, 21);
            this.label8.TabIndex = 59;
            this.label8.Text = " Cursos Disponibles";
            // 
            // cbCursosDisponibles
            // 
            this.cbCursosDisponibles.DataSource = this.eCursoBindingSource;
            this.cbCursosDisponibles.FormattingEnabled = true;
            this.cbCursosDisponibles.Location = new System.Drawing.Point(180, 56);
            this.cbCursosDisponibles.Name = "cbCursosDisponibles";
            this.cbCursosDisponibles.Size = new System.Drawing.Size(592, 29);
            this.cbCursosDisponibles.TabIndex = 58;
            // 
            // txtmTeleD
            // 
            this.txtmTeleD.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmTeleD.Location = new System.Drawing.Point(180, 167);
            this.txtmTeleD.Mask = "########";
            this.txtmTeleD.Name = "txtmTeleD";
            this.txtmTeleD.Size = new System.Drawing.Size(180, 27);
            this.txtmTeleD.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(176, 143);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 21);
            this.label7.TabIndex = 11;
            this.label7.Text = "Teléfono";
            // 
            // txtEmailD
            // 
            this.txtEmailD.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmailD.Location = new System.Drawing.Point(180, 115);
            this.txtEmailD.Multiline = true;
            this.txtEmailD.Name = "txtEmailD";
            this.txtEmailD.Size = new System.Drawing.Size(180, 27);
            this.txtEmailD.TabIndex = 10;
            this.txtEmailD.Text = " ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(176, 91);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 21);
            this.label6.TabIndex = 9;
            this.label6.Text = "Email";
            // 
            // txtxApellidoD
            // 
            this.txtxApellidoD.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtxApellidoD.Location = new System.Drawing.Point(6, 219);
            this.txtxApellidoD.Multiline = true;
            this.txtxApellidoD.Name = "txtxApellidoD";
            this.txtxApellidoD.Size = new System.Drawing.Size(151, 27);
            this.txtxApellidoD.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(2, 195);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 21);
            this.label5.TabIndex = 7;
            this.label5.Text = " Apellido";
            // 
            // txtNombreD
            // 
            this.txtNombreD.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreD.Location = new System.Drawing.Point(6, 167);
            this.txtNombreD.Multiline = true;
            this.txtNombreD.Name = "txtNombreD";
            this.txtNombreD.Size = new System.Drawing.Size(151, 27);
            this.txtNombreD.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 21);
            this.label4.TabIndex = 5;
            this.label4.Text = " Nombre";
            // 
            // txtmCedD
            // 
            this.txtmCedD.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmCedD.Location = new System.Drawing.Point(6, 110);
            this.txtmCedD.Mask = "#-####-####";
            this.txtmCedD.Name = "txtmCedD";
            this.txtmCedD.Size = new System.Drawing.Size(151, 27);
            this.txtmCedD.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 21);
            this.label3.TabIndex = 3;
            this.label3.Text = "Cedula";
            // 
            // txtmPinD
            // 
            this.txtmPinD.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmPinD.Location = new System.Drawing.Point(6, 58);
            this.txtmPinD.Mask = "####";
            this.txtmPinD.Name = "txtmPinD";
            this.txtmPinD.Size = new System.Drawing.Size(151, 27);
            this.txtmPinD.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "PIN";
            // 
            // btnElimiDgvD
            // 
            this.btnElimiDgvD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnElimiDgvD.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnElimiDgvD.ForeColor = System.Drawing.Color.White;
            this.btnElimiDgvD.Location = new System.Drawing.Point(584, 14);
            this.btnElimiDgvD.Name = "btnElimiDgvD";
            this.btnElimiDgvD.Size = new System.Drawing.Size(99, 30);
            this.btnElimiDgvD.TabIndex = 3;
            this.btnElimiDgvD.Text = "Eliminar";
            this.btnElimiDgvD.UseVisualStyleBackColor = true;
            this.btnElimiDgvD.Click += new System.EventHandler(this.btnElimiDgvD_Click);
            // 
            // btnCargaDgvD
            // 
            this.btnCargaDgvD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCargaDgvD.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCargaDgvD.ForeColor = System.Drawing.Color.White;
            this.btnCargaDgvD.Location = new System.Drawing.Point(689, 14);
            this.btnCargaDgvD.Name = "btnCargaDgvD";
            this.btnCargaDgvD.Size = new System.Drawing.Size(99, 30);
            this.btnCargaDgvD.TabIndex = 2;
            this.btnCargaDgvD.Text = "Cargar";
            this.btnCargaDgvD.UseVisualStyleBackColor = true;
            this.btnCargaDgvD.Click += new System.EventHandler(this.btnCargaDgvD_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = " Docentes";
            // 
            // dgvDocentes
            // 
            this.dgvDocentes.AllowUserToAddRows = false;
            this.dgvDocentes.AutoGenerateColumns = false;
            this.dgvDocentes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDocentes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.pinDataGridViewTextBoxColumn,
            this.cedulaDataGridViewTextBoxColumn,
            this.nombreDataGridViewTextBoxColumn1,
            this.apellidoDataGridViewTextBoxColumn,
            this.emailDataGridViewTextBoxColumn,
            this.telefonoDataGridViewTextBoxColumn,
            this.cantidadTDataGridViewTextBoxColumn,
            this.cantidadADataGridViewTextBoxColumn,
            this.activoDataGridViewTextBoxColumn1});
            this.dgvDocentes.DataSource = this.eDocenteBindingSource;
            this.dgvDocentes.Location = new System.Drawing.Point(7, 50);
            this.dgvDocentes.Name = "dgvDocentes";
            this.dgvDocentes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDocentes.Size = new System.Drawing.Size(781, 116);
            this.dgvDocentes.TabIndex = 0;
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.idDataGridViewTextBoxColumn1.Visible = false;
            this.idDataGridViewTextBoxColumn1.Width = 60;
            // 
            // pinDataGridViewTextBoxColumn
            // 
            this.pinDataGridViewTextBoxColumn.DataPropertyName = "Pin";
            this.pinDataGridViewTextBoxColumn.HeaderText = "Pin";
            this.pinDataGridViewTextBoxColumn.Name = "pinDataGridViewTextBoxColumn";
            this.pinDataGridViewTextBoxColumn.Width = 60;
            // 
            // cedulaDataGridViewTextBoxColumn
            // 
            this.cedulaDataGridViewTextBoxColumn.DataPropertyName = "Cedula";
            this.cedulaDataGridViewTextBoxColumn.HeaderText = "Cedula";
            this.cedulaDataGridViewTextBoxColumn.Name = "cedulaDataGridViewTextBoxColumn";
            // 
            // nombreDataGridViewTextBoxColumn1
            // 
            this.nombreDataGridViewTextBoxColumn1.DataPropertyName = "Nombre";
            this.nombreDataGridViewTextBoxColumn1.HeaderText = "Nombre";
            this.nombreDataGridViewTextBoxColumn1.Name = "nombreDataGridViewTextBoxColumn1";
            // 
            // apellidoDataGridViewTextBoxColumn
            // 
            this.apellidoDataGridViewTextBoxColumn.DataPropertyName = "Apellido";
            this.apellidoDataGridViewTextBoxColumn.HeaderText = "Apellido";
            this.apellidoDataGridViewTextBoxColumn.Name = "apellidoDataGridViewTextBoxColumn";
            // 
            // emailDataGridViewTextBoxColumn
            // 
            this.emailDataGridViewTextBoxColumn.DataPropertyName = "Email";
            this.emailDataGridViewTextBoxColumn.HeaderText = "Email";
            this.emailDataGridViewTextBoxColumn.Name = "emailDataGridViewTextBoxColumn";
            // 
            // telefonoDataGridViewTextBoxColumn
            // 
            this.telefonoDataGridViewTextBoxColumn.DataPropertyName = "Telefono";
            this.telefonoDataGridViewTextBoxColumn.HeaderText = "Telefono";
            this.telefonoDataGridViewTextBoxColumn.Name = "telefonoDataGridViewTextBoxColumn";
            // 
            // cantidadTDataGridViewTextBoxColumn
            // 
            this.cantidadTDataGridViewTextBoxColumn.DataPropertyName = "CantidadT";
            this.cantidadTDataGridViewTextBoxColumn.HeaderText = "CantidadT";
            this.cantidadTDataGridViewTextBoxColumn.Name = "cantidadTDataGridViewTextBoxColumn";
            this.cantidadTDataGridViewTextBoxColumn.Width = 95;
            // 
            // cantidadADataGridViewTextBoxColumn
            // 
            this.cantidadADataGridViewTextBoxColumn.DataPropertyName = "CantidadA";
            this.cantidadADataGridViewTextBoxColumn.HeaderText = "CantidadA";
            this.cantidadADataGridViewTextBoxColumn.Name = "cantidadADataGridViewTextBoxColumn";
            this.cantidadADataGridViewTextBoxColumn.Width = 90;
            // 
            // activoDataGridViewTextBoxColumn1
            // 
            this.activoDataGridViewTextBoxColumn1.DataPropertyName = "Activo";
            this.activoDataGridViewTextBoxColumn1.HeaderText = "Activo";
            this.activoDataGridViewTextBoxColumn1.Name = "activoDataGridViewTextBoxColumn1";
            this.activoDataGridViewTextBoxColumn1.Visible = false;
            // 
            // eDocenteBindingSource
            // 
            this.eDocenteBindingSource.DataSource = typeof(RelojMarcadorENL.EDocente);
            // 
            // aTabHorario
            // 
            this.aTabHorario.BackColor = System.Drawing.Color.Silver;
            this.aTabHorario.Controls.Add(this.btnNuevoH);
            this.aTabHorario.Controls.Add(this.dgvHorarios);
            this.aTabHorario.Controls.Add(this.gpHorario);
            this.aTabHorario.Controls.Add(this.btnElimiDgvH);
            this.aTabHorario.Controls.Add(this.btnCargaDgvH);
            this.aTabHorario.Controls.Add(this.label9);
            this.aTabHorario.Location = new System.Drawing.Point(4, 5);
            this.aTabHorario.Name = "aTabHorario";
            this.aTabHorario.Padding = new System.Windows.Forms.Padding(3);
            this.aTabHorario.Size = new System.Drawing.Size(794, 434);
            this.aTabHorario.TabIndex = 1;
            this.aTabHorario.Text = "tabPage2";
            // 
            // btnNuevoH
            // 
            this.btnNuevoH.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevoH.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevoH.ForeColor = System.Drawing.Color.White;
            this.btnNuevoH.Location = new System.Drawing.Point(479, 14);
            this.btnNuevoH.Name = "btnNuevoH";
            this.btnNuevoH.Size = new System.Drawing.Size(99, 30);
            this.btnNuevoH.TabIndex = 9;
            this.btnNuevoH.Text = "Nuevo";
            this.btnNuevoH.UseVisualStyleBackColor = true;
            this.btnNuevoH.Click += new System.EventHandler(this.btnNuevoH_Click);
            // 
            // dgvHorarios
            // 
            this.dgvHorarios.AutoGenerateColumns = false;
            this.dgvHorarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHorarios.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.dataGridViewTextBoxColumn1,
            this.horaInicioDataGridViewTextBoxColumn,
            this.horaFinalDataGridViewTextBoxColumn,
            this.activoDataGridViewTextBoxColumn});
            this.dgvHorarios.DataSource = this.eHorarioBindingSource1;
            this.dgvHorarios.Location = new System.Drawing.Point(6, 47);
            this.dgvHorarios.Name = "dgvHorarios";
            this.dgvHorarios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvHorarios.Size = new System.Drawing.Size(782, 119);
            this.dgvHorarios.TabIndex = 8;
            this.dgvHorarios.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvHorarios_CellContentClick);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Dia";
            this.dataGridViewTextBoxColumn1.HeaderText = "Dia";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // horaInicioDataGridViewTextBoxColumn
            // 
            this.horaInicioDataGridViewTextBoxColumn.DataPropertyName = "HoraInicio";
            this.horaInicioDataGridViewTextBoxColumn.HeaderText = "HoraInicio";
            this.horaInicioDataGridViewTextBoxColumn.Name = "horaInicioDataGridViewTextBoxColumn";
            // 
            // horaFinalDataGridViewTextBoxColumn
            // 
            this.horaFinalDataGridViewTextBoxColumn.DataPropertyName = "HoraFinal";
            this.horaFinalDataGridViewTextBoxColumn.HeaderText = "HoraFinal";
            this.horaFinalDataGridViewTextBoxColumn.Name = "horaFinalDataGridViewTextBoxColumn";
            // 
            // activoDataGridViewTextBoxColumn
            // 
            this.activoDataGridViewTextBoxColumn.DataPropertyName = "Activo";
            this.activoDataGridViewTextBoxColumn.HeaderText = "Activo";
            this.activoDataGridViewTextBoxColumn.Name = "activoDataGridViewTextBoxColumn";
            this.activoDataGridViewTextBoxColumn.Visible = false;
            // 
            // eHorarioBindingSource1
            // 
            this.eHorarioBindingSource1.DataSource = typeof(RelojMarcadorENL.EHorario);
            // 
            // gpHorario
            // 
            this.gpHorario.Controls.Add(this.btnAgregarHorario);
            this.gpHorario.Controls.Add(this.lblErrorH);
            this.gpHorario.Controls.Add(this.cbHoraFinal);
            this.gpHorario.Controls.Add(this.cbHoraInicial);
            this.gpHorario.Controls.Add(this.cbDiaHorario);
            this.gpHorario.Controls.Add(this.label29);
            this.gpHorario.Controls.Add(this.label30);
            this.gpHorario.Controls.Add(this.label31);
            this.gpHorario.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpHorario.ForeColor = System.Drawing.Color.White;
            this.gpHorario.Location = new System.Drawing.Point(10, 172);
            this.gpHorario.Name = "gpHorario";
            this.gpHorario.Size = new System.Drawing.Size(778, 256);
            this.gpHorario.TabIndex = 7;
            this.gpHorario.TabStop = false;
            this.gpHorario.Text = "Horario";
            // 
            // btnAgregarHorario
            // 
            this.btnAgregarHorario.BackColor = System.Drawing.Color.Silver;
            this.btnAgregarHorario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAgregarHorario.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnAgregarHorario.FlatAppearance.BorderSize = 0;
            this.btnAgregarHorario.ForeColor = System.Drawing.Color.White;
            this.btnAgregarHorario.Location = new System.Drawing.Point(10, 106);
            this.btnAgregarHorario.Name = "btnAgregarHorario";
            this.btnAgregarHorario.Size = new System.Drawing.Size(196, 29);
            this.btnAgregarHorario.TabIndex = 60;
            this.btnAgregarHorario.Text = "Agregar";
            this.btnAgregarHorario.UseVisualStyleBackColor = false;
            this.btnAgregarHorario.Click += new System.EventHandler(this.btnAgregarHorario_Click);
            // 
            // lblErrorH
            // 
            this.lblErrorH.AutoSize = true;
            this.lblErrorH.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorH.ForeColor = System.Drawing.Color.White;
            this.lblErrorH.Location = new System.Drawing.Point(6, 223);
            this.lblErrorH.Name = "lblErrorH";
            this.lblErrorH.Size = new System.Drawing.Size(79, 21);
            this.lblErrorH.TabIndex = 59;
            this.lblErrorH.Text = " Mensaje";
            // 
            // cbHoraFinal
            // 
            this.cbHoraFinal.FormattingEnabled = true;
            this.cbHoraFinal.Items.AddRange(new object[] {
            "8:00",
            "9:00",
            "10:00",
            "11:30",
            "//Tarde//",
            "13:00",
            "14:00",
            "15:00",
            "16:30",
            "//Noche//",
            "18:00",
            "19:00",
            "20:00",
            "21:00",
            "22:00"});
            this.cbHoraFinal.Location = new System.Drawing.Point(444, 54);
            this.cbHoraFinal.Name = "cbHoraFinal";
            this.cbHoraFinal.Size = new System.Drawing.Size(196, 29);
            this.cbHoraFinal.TabIndex = 58;
            this.cbHoraFinal.SelectedIndexChanged += new System.EventHandler(this.cbHoraFinal_SelectedIndexChanged);
            // 
            // cbHoraInicial
            // 
            this.cbHoraInicial.FormattingEnabled = true;
            this.cbHoraInicial.Items.AddRange(new object[] {
            "8:00",
            "9:00",
            "10:00",
            "11:30",
            "//Tarde//",
            "13:00",
            "14:00",
            "15:00",
            "16:30",
            "//Noche//",
            "18:00",
            "19:00",
            "20:00",
            "21:00",
            "22:00"});
            this.cbHoraInicial.Location = new System.Drawing.Point(230, 54);
            this.cbHoraInicial.Name = "cbHoraInicial";
            this.cbHoraInicial.Size = new System.Drawing.Size(196, 29);
            this.cbHoraInicial.TabIndex = 57;
            this.cbHoraInicial.SelectedIndexChanged += new System.EventHandler(this.cbHoraInicial_SelectedIndexChanged);
            // 
            // cbDiaHorario
            // 
            this.cbDiaHorario.FormattingEnabled = true;
            this.cbDiaHorario.Items.AddRange(new object[] {
            "Lunes",
            "Martes",
            "Miercoles",
            "Jueves",
            "Viernes",
            "Lunes/Martes",
            "Lunes/Miercoles",
            "Lunes/Jueves",
            "Lunes/Viernes",
            "Martes/Miercoles",
            "Martes/Jueves",
            "Martes/Viernes",
            "Miercoles/Jueves",
            "Miercoles/Viernes",
            "Jueves/Viernes"});
            this.cbDiaHorario.Location = new System.Drawing.Point(10, 54);
            this.cbDiaHorario.Name = "cbDiaHorario";
            this.cbDiaHorario.Size = new System.Drawing.Size(196, 29);
            this.cbDiaHorario.TabIndex = 56;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.White;
            this.label29.Location = new System.Drawing.Point(440, 23);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(123, 21);
            this.label29.TabIndex = 53;
            this.label29.Text = "Hora de Salida";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.White;
            this.label30.Location = new System.Drawing.Point(226, 30);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(118, 21);
            this.label30.TabIndex = 54;
            this.label30.Text = "Hora De Inicio";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.White;
            this.label31.Location = new System.Drawing.Point(6, 30);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(36, 21);
            this.label31.TabIndex = 55;
            this.label31.Text = "Día";
            // 
            // btnElimiDgvH
            // 
            this.btnElimiDgvH.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnElimiDgvH.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnElimiDgvH.ForeColor = System.Drawing.Color.White;
            this.btnElimiDgvH.Location = new System.Drawing.Point(584, 14);
            this.btnElimiDgvH.Name = "btnElimiDgvH";
            this.btnElimiDgvH.Size = new System.Drawing.Size(99, 30);
            this.btnElimiDgvH.TabIndex = 6;
            this.btnElimiDgvH.Text = "Eliminar";
            this.btnElimiDgvH.UseVisualStyleBackColor = true;
            this.btnElimiDgvH.Click += new System.EventHandler(this.btnElimiDgvH_Click);
            // 
            // btnCargaDgvH
            // 
            this.btnCargaDgvH.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCargaDgvH.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCargaDgvH.ForeColor = System.Drawing.Color.White;
            this.btnCargaDgvH.Location = new System.Drawing.Point(689, 14);
            this.btnCargaDgvH.Name = "btnCargaDgvH";
            this.btnCargaDgvH.Size = new System.Drawing.Size(99, 30);
            this.btnCargaDgvH.TabIndex = 5;
            this.btnCargaDgvH.Text = "Cargar";
            this.btnCargaDgvH.UseVisualStyleBackColor = true;
            this.btnCargaDgvH.Click += new System.EventHandler(this.btnCargaDgvH_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(6, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 21);
            this.label9.TabIndex = 4;
            this.label9.Text = "Horarios";
            // 
            // aTabCurso
            // 
            this.aTabCurso.BackColor = System.Drawing.Color.Silver;
            this.aTabCurso.Controls.Add(this.btnNuevoC);
            this.aTabCurso.Controls.Add(this.gpCurso);
            this.aTabCurso.Controls.Add(this.btnElimiDgvC);
            this.aTabCurso.Controls.Add(this.btnCargaDgvC);
            this.aTabCurso.Controls.Add(this.label10);
            this.aTabCurso.Controls.Add(this.dgvCursos);
            this.aTabCurso.Location = new System.Drawing.Point(4, 5);
            this.aTabCurso.Name = "aTabCurso";
            this.aTabCurso.Padding = new System.Windows.Forms.Padding(3);
            this.aTabCurso.Size = new System.Drawing.Size(794, 434);
            this.aTabCurso.TabIndex = 2;
            this.aTabCurso.Text = "tabPage3";
            // 
            // btnNuevoC
            // 
            this.btnNuevoC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevoC.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevoC.ForeColor = System.Drawing.Color.White;
            this.btnNuevoC.Location = new System.Drawing.Point(480, 14);
            this.btnNuevoC.Name = "btnNuevoC";
            this.btnNuevoC.Size = new System.Drawing.Size(99, 30);
            this.btnNuevoC.TabIndex = 69;
            this.btnNuevoC.Text = "Nuevo";
            this.btnNuevoC.UseVisualStyleBackColor = true;
            this.btnNuevoC.Click += new System.EventHandler(this.btnNuevoC_Click);
            // 
            // gpCurso
            // 
            this.gpCurso.Controls.Add(this.txtmCupo);
            this.gpCurso.Controls.Add(this.txtmAula);
            this.gpCurso.Controls.Add(this.btnAgregarCurso);
            this.gpCurso.Controls.Add(this.lblErrorC);
            this.gpCurso.Controls.Add(this.label16);
            this.gpCurso.Controls.Add(this.cbHorariosC);
            this.gpCurso.Controls.Add(this.label15);
            this.gpCurso.Controls.Add(this.label14);
            this.gpCurso.Controls.Add(this.dtpFinal);
            this.gpCurso.Controls.Add(this.dtpInicio);
            this.gpCurso.Controls.Add(this.label12);
            this.gpCurso.Controls.Add(this.txtNombreC);
            this.gpCurso.Controls.Add(this.label11);
            this.gpCurso.Controls.Add(this.label13);
            this.gpCurso.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpCurso.ForeColor = System.Drawing.Color.White;
            this.gpCurso.Location = new System.Drawing.Point(10, 172);
            this.gpCurso.Name = "gpCurso";
            this.gpCurso.Size = new System.Drawing.Size(778, 256);
            this.gpCurso.TabIndex = 7;
            this.gpCurso.TabStop = false;
            this.gpCurso.Text = "Curso";
            // 
            // txtmCupo
            // 
            this.txtmCupo.Location = new System.Drawing.Point(288, 108);
            this.txtmCupo.Mask = "##";
            this.txtmCupo.Name = "txtmCupo";
            this.txtmCupo.Size = new System.Drawing.Size(209, 27);
            this.txtmCupo.TabIndex = 68;
            // 
            // txtmAula
            // 
            this.txtmAula.Location = new System.Drawing.Point(10, 108);
            this.txtmAula.Mask = "A-##";
            this.txtmAula.Name = "txtmAula";
            this.txtmAula.Size = new System.Drawing.Size(272, 27);
            this.txtmAula.TabIndex = 67;
            // 
            // btnAgregarCurso
            // 
            this.btnAgregarCurso.BackColor = System.Drawing.Color.Silver;
            this.btnAgregarCurso.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAgregarCurso.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnAgregarCurso.FlatAppearance.BorderSize = 0;
            this.btnAgregarCurso.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregarCurso.ForeColor = System.Drawing.Color.White;
            this.btnAgregarCurso.Location = new System.Drawing.Point(288, 149);
            this.btnAgregarCurso.Name = "btnAgregarCurso";
            this.btnAgregarCurso.Size = new System.Drawing.Size(201, 33);
            this.btnAgregarCurso.TabIndex = 66;
            this.btnAgregarCurso.Text = "Agregar";
            this.btnAgregarCurso.UseVisualStyleBackColor = false;
            this.btnAgregarCurso.Click += new System.EventHandler(this.btnAgregarCurso_Click);
            // 
            // lblErrorC
            // 
            this.lblErrorC.AutoSize = true;
            this.lblErrorC.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorC.ForeColor = System.Drawing.Color.White;
            this.lblErrorC.Location = new System.Drawing.Point(286, 185);
            this.lblErrorC.Name = "lblErrorC";
            this.lblErrorC.Size = new System.Drawing.Size(64, 17);
            this.lblErrorC.TabIndex = 65;
            this.lblErrorC.Text = " Mensaje";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(285, 34);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 21);
            this.label16.TabIndex = 64;
            this.label16.Text = "Horario";
            // 
            // cbHorariosC
            // 
            this.cbHorariosC.DataSource = this.eHorarioBindingSource;
            this.cbHorariosC.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHorariosC.FormattingEnabled = true;
            this.cbHorariosC.Location = new System.Drawing.Point(288, 58);
            this.cbHorariosC.Name = "cbHorariosC";
            this.cbHorariosC.Size = new System.Drawing.Size(209, 25);
            this.cbHorariosC.TabIndex = 63;
            // 
            // eHorarioBindingSource
            // 
            this.eHorarioBindingSource.DataSource = typeof(RelojMarcadorENL.EHorario);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(6, 194);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(102, 21);
            this.label15.TabIndex = 62;
            this.label15.Text = " Fecha Final";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(6, 130);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(129, 21);
            this.label14.TabIndex = 61;
            this.label14.Text = "Fecha de Inicio";
            // 
            // dtpFinal
            // 
            this.dtpFinal.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFinal.Location = new System.Drawing.Point(10, 218);
            this.dtpFinal.Name = "dtpFinal";
            this.dtpFinal.Size = new System.Drawing.Size(272, 23);
            this.dtpFinal.TabIndex = 60;
            // 
            // dtpInicio
            // 
            this.dtpInicio.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpInicio.Location = new System.Drawing.Point(10, 154);
            this.dtpInicio.Name = "dtpInicio";
            this.dtpInicio.Size = new System.Drawing.Size(272, 23);
            this.dtpInicio.TabIndex = 59;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(285, 86);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(58, 21);
            this.label12.TabIndex = 57;
            this.label12.Text = " Cupo";
            // 
            // txtNombreC
            // 
            this.txtNombreC.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreC.Location = new System.Drawing.Point(10, 58);
            this.txtNombreC.Multiline = true;
            this.txtNombreC.Name = "txtNombreC";
            this.txtNombreC.Size = new System.Drawing.Size(272, 25);
            this.txtNombreC.TabIndex = 56;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(6, 84);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 21);
            this.label11.TabIndex = 55;
            this.label11.Text = " Aula";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(6, 34);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 21);
            this.label13.TabIndex = 53;
            this.label13.Text = " Nombre";
            // 
            // btnElimiDgvC
            // 
            this.btnElimiDgvC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnElimiDgvC.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnElimiDgvC.ForeColor = System.Drawing.Color.White;
            this.btnElimiDgvC.Location = new System.Drawing.Point(585, 14);
            this.btnElimiDgvC.Name = "btnElimiDgvC";
            this.btnElimiDgvC.Size = new System.Drawing.Size(99, 30);
            this.btnElimiDgvC.TabIndex = 6;
            this.btnElimiDgvC.Text = "Eliminar";
            this.btnElimiDgvC.UseVisualStyleBackColor = true;
            this.btnElimiDgvC.Click += new System.EventHandler(this.btnElimiDgvC_Click);
            // 
            // btnCargaDgvC
            // 
            this.btnCargaDgvC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCargaDgvC.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCargaDgvC.ForeColor = System.Drawing.Color.White;
            this.btnCargaDgvC.Location = new System.Drawing.Point(690, 14);
            this.btnCargaDgvC.Name = "btnCargaDgvC";
            this.btnCargaDgvC.Size = new System.Drawing.Size(99, 30);
            this.btnCargaDgvC.TabIndex = 5;
            this.btnCargaDgvC.Text = "Cargar";
            this.btnCargaDgvC.UseVisualStyleBackColor = true;
            this.btnCargaDgvC.Click += new System.EventHandler(this.btnCargaDgvC_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(6, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 21);
            this.label10.TabIndex = 4;
            this.label10.Text = "Cursos";
            // 
            // dgvCursos
            // 
            this.dgvCursos.AutoGenerateColumns = false;
            this.dgvCursos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCursos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn2,
            this.nombreDataGridViewTextBoxColumn,
            this.aulaDataGridViewTextBoxColumn,
            this.fechaInicioDataGridViewTextBoxColumn,
            this.fechaFinalDataGridViewTextBoxColumn,
            this.cupoDataGridViewTextBoxColumn,
            this.horarioDataGridViewTextBoxColumn,
            this.activoDataGridViewTextBoxColumn2});
            this.dgvCursos.DataSource = this.eCursoBindingSource1;
            this.dgvCursos.Location = new System.Drawing.Point(7, 50);
            this.dgvCursos.Name = "dgvCursos";
            this.dgvCursos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCursos.Size = new System.Drawing.Size(781, 116);
            this.dgvCursos.TabIndex = 1;
            // 
            // idDataGridViewTextBoxColumn2
            // 
            this.idDataGridViewTextBoxColumn2.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn2.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn2.Name = "idDataGridViewTextBoxColumn2";
            this.idDataGridViewTextBoxColumn2.Visible = false;
            // 
            // nombreDataGridViewTextBoxColumn
            // 
            this.nombreDataGridViewTextBoxColumn.DataPropertyName = "Nombre";
            this.nombreDataGridViewTextBoxColumn.HeaderText = "Nombre";
            this.nombreDataGridViewTextBoxColumn.Name = "nombreDataGridViewTextBoxColumn";
            // 
            // aulaDataGridViewTextBoxColumn
            // 
            this.aulaDataGridViewTextBoxColumn.DataPropertyName = "Aula";
            this.aulaDataGridViewTextBoxColumn.HeaderText = "Aula";
            this.aulaDataGridViewTextBoxColumn.Name = "aulaDataGridViewTextBoxColumn";
            // 
            // fechaInicioDataGridViewTextBoxColumn
            // 
            this.fechaInicioDataGridViewTextBoxColumn.DataPropertyName = "FechaInicio";
            this.fechaInicioDataGridViewTextBoxColumn.HeaderText = "FechaInicio";
            this.fechaInicioDataGridViewTextBoxColumn.Name = "fechaInicioDataGridViewTextBoxColumn";
            this.fechaInicioDataGridViewTextBoxColumn.Width = 150;
            // 
            // fechaFinalDataGridViewTextBoxColumn
            // 
            this.fechaFinalDataGridViewTextBoxColumn.DataPropertyName = "FechaFinal";
            this.fechaFinalDataGridViewTextBoxColumn.HeaderText = "FechaFinal";
            this.fechaFinalDataGridViewTextBoxColumn.Name = "fechaFinalDataGridViewTextBoxColumn";
            this.fechaFinalDataGridViewTextBoxColumn.Width = 150;
            // 
            // cupoDataGridViewTextBoxColumn
            // 
            this.cupoDataGridViewTextBoxColumn.DataPropertyName = "Cupo";
            this.cupoDataGridViewTextBoxColumn.HeaderText = "Cupo";
            this.cupoDataGridViewTextBoxColumn.Name = "cupoDataGridViewTextBoxColumn";
            this.cupoDataGridViewTextBoxColumn.Width = 80;
            // 
            // horarioDataGridViewTextBoxColumn
            // 
            this.horarioDataGridViewTextBoxColumn.DataPropertyName = "Horario";
            this.horarioDataGridViewTextBoxColumn.HeaderText = "Horario";
            this.horarioDataGridViewTextBoxColumn.Name = "horarioDataGridViewTextBoxColumn";
            this.horarioDataGridViewTextBoxColumn.Width = 155;
            // 
            // activoDataGridViewTextBoxColumn2
            // 
            this.activoDataGridViewTextBoxColumn2.DataPropertyName = "Activo";
            this.activoDataGridViewTextBoxColumn2.FillWeight = 120F;
            this.activoDataGridViewTextBoxColumn2.HeaderText = "Activo";
            this.activoDataGridViewTextBoxColumn2.Name = "activoDataGridViewTextBoxColumn2";
            this.activoDataGridViewTextBoxColumn2.Visible = false;
            // 
            // eCursoBindingSource1
            // 
            this.eCursoBindingSource1.DataSource = typeof(RelojMarcadorENL.ECurso);
            // 
            // FrmEditar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(844, 511);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Name = "FrmEditar";
            this.Text = "FrmEditar";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmEditar_FormClosing);
            this.Load += new System.EventHandler(this.FrmEditar_Load);
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.aTabDocente.ResumeLayout(false);
            this.aTabDocente.PerformLayout();
            this.gpDocente.ResumeLayout(false);
            this.gpDocente.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCursosDocente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eCursoBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eCursoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocentes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eDocenteBindingSource)).EndInit();
            this.aTabHorario.ResumeLayout(false);
            this.aTabHorario.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHorarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eHorarioBindingSource1)).EndInit();
            this.gpHorario.ResumeLayout(false);
            this.gpHorario.PerformLayout();
            this.aTabCurso.ResumeLayout(false);
            this.aTabCurso.PerformLayout();
            this.gpCurso.ResumeLayout(false);
            this.gpCurso.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eHorarioBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCursos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eCursoBindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnCargarC;
        private System.Windows.Forms.Button btnCargarD;
        private System.Windows.Forms.Button btnCargarH;
        private System.Windows.Forms.DataGridViewTextBoxColumn diaDataGridViewTextBoxColumn;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage aTabDocente;
        private System.Windows.Forms.TabPage aTabHorario;
        private System.Windows.Forms.TabPage aTabCurso;
        private System.Windows.Forms.DataGridView dgvDocentes;
        private System.Windows.Forms.DataGridView dgvCursos;
        private System.Windows.Forms.Button btnElimiDgvD;
        private System.Windows.Forms.Button btnCargaDgvD;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gpDocente;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox txtmPinD;
        private System.Windows.Forms.MaskedTextBox txtmCedD;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNombreD;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtxApellidoD;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MaskedTextBox txtmTeleD;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtEmailD;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnAgregarDocente;
        private System.Windows.Forms.Button btnCargarDgvCD;
        private System.Windows.Forms.DataGridView dgvCursosDocente;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbCursosDisponibles;
        private System.Windows.Forms.GroupBox gpHorario;
        private System.Windows.Forms.Button btnElimiDgvH;
        private System.Windows.Forms.Button btnCargaDgvH;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnAgregarHorario;
        private System.Windows.Forms.Label lblErrorH;
        private System.Windows.Forms.ComboBox cbHoraFinal;
        private System.Windows.Forms.ComboBox cbHoraInicial;
        private System.Windows.Forms.ComboBox cbDiaHorario;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button btnElimiDgvC;
        private System.Windows.Forms.Button btnCargaDgvC;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox gpCurso;
        private System.Windows.Forms.MaskedTextBox txtmAula;
        private System.Windows.Forms.Button btnAgregarCurso;
        private System.Windows.Forms.Label lblErrorC;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cbHorariosC;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker dtpFinal;
        private System.Windows.Forms.DateTimePicker dtpInicio;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtNombreC;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox txtmCupo;
        private System.Windows.Forms.Label lblErrorD;
        private System.Windows.Forms.BindingSource eHorarioBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn pinDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cedulaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn apellidoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn emailDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn telefonoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidadTDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidadADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn activoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.BindingSource eDocenteBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn horaIniciioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colid;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn aulaDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaInicioDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaFinalDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cupoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn horarioDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn activoDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridView dgvHorarios;
        private System.Windows.Forms.BindingSource eCursoBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn horaInicioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn horaFinalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn activoDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource eHorarioBindingSource1;
        private System.Windows.Forms.BindingSource eCursoBindingSource1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn aulaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaInicioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaFinalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cupoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn horarioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn activoDataGridViewTextBoxColumn2;
        private System.Windows.Forms.BindingSource eCursoBindingSource2;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn aulaDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaInicioDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaFinalDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn cupoDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn horarioDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn activoDataGridViewTextBoxColumn4;
        private System.Windows.Forms.Button btnNuevoD;
        private System.Windows.Forms.Button btnNuevoH;
        private System.Windows.Forms.Button btnNuevoC;
    }
}