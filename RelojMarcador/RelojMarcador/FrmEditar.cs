﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RelojMarcadorBOL;
using RelojMarcadorENL;

namespace RelojMarcador
{
    public partial class FrmEditar : Form
    {
        private DocenteBOL dbol;
        private CursoBOL cbol;
        private HorarioBOL hbol;
        private EDocente docente;
        private EHorario horario;
        private ECurso curso;
        List<ECurso> listaDeCursos;

        public FrmEditar()
        {
            InitializeComponent();
            CenterToScreen();
            Colores();
        }



        private void FrmEditar_Load(object sender, EventArgs e)
        {
            dbol = new DocenteBOL();
            cbol = new CursoBOL();
            hbol = new HorarioBOL();
            docente = new EDocente();
            horario = new EHorario();
            curso = new ECurso();
            listaDeCursos = new List<ECurso>();

        }

        private void btnCargarD_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;
            CargarDocentes(dgvDocentes);
            cbCursosDisponibles.DataSource = cbol.CargarTodos();
        }

        private void btnCargarH_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 1;
            CargarHorario(dgvHorarios);
        }

        private void btnCargarC_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 2;
            CargarCursos(dgvCursos);
            cbHorariosC.DataSource = hbol.CargaTodos();

        }

        private void FrmEditar_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Owner != null)
            {
                Owner.Show();
                Owner.Refresh();
            }
        }

        private void btnAgregarDocente_Click(object sender, EventArgs e)
        {
            try
            {
                docente.Pin = txtmPinD.Text.Equals("") ? 0 : Convert.ToInt32(txtmPinD.Text.Trim());
                docente.Cedula = txtmCedD.Text.Trim();
                docente.Nombre = txtNombreD.Text.Trim();
                docente.Apellido = txtxApellidoD.Text.Trim();
                docente.Email = txtEmailD.Text.Trim();
                docente.Telefono = txtmTeleD.Text.Trim();
                docente.CantidadA = 0;
                docente.CantidadT = 0;
                docente.Cursos = listaDeCursos;
                if (dbol.Agregar(docente))
                {
                    Limpiar(gpDocente);
                    lblErrorD.Text = "Se Agrego con Exito";
                    CargarDocentes(dgvDocentes);
                }
            }
            catch (Exception ex)
            {

                lblErrorD.Text = ex.Message;

            }
        }

        private void btnAgregarHorario_Click(object sender, EventArgs e)
        {
            try
            {
                horario.Dia = cbDiaHorario.Text;
                horario.HoraInicio = cbHoraInicial.Text;
                horario.HoraFinal = cbHoraFinal.Text;
                Console.WriteLine(horario.HoraFinal);
                horario.Activo = 1;
                if (HorasValidas(horario.HoraInicio, horario.HoraFinal))
                {
                    if (hbol.Agregar(horario))
                    {
                        Limpiar(gpHorario);
                        lblErrorH.Text = "Se Agrego Con Exito";
                        CargarHorario(dgvHorarios);
                    }
                }
            }
            catch (Exception ex)
            {

                lblErrorH.Text = ex.Message + "\n" + "Intentlo Nuevamente.";
            }
        }

        private void btnAgregarCurso_Click(object sender, EventArgs e)
        {
            try
            {
                curso.Nombre = txtNombreC.Text.Trim();
                curso.Aula = txtmAula.Text.Trim();
                curso.FechaInicio = dtpInicio.Value;
                curso.FechaFinal = dtpFinal.Value;
                curso.Cupo = IngresarCupo(txtmCupo.Text.Trim());
                curso.Horario = cbHorariosC.SelectedItem as EHorario;
                if (cbol.Agregar(curso))
                {
                    Limpiar(gpCurso);
                    lblErrorC.Text = "Curso Agregado con Exito";
                    CargarCursos(dgvCursos);
                }
            }

            catch (Exception ex)
            {

                lblErrorC.Text = ex.Message;
            }
        }

        private void btnCargarDgvCD_Click(object sender, EventArgs e)
        {
            try
            {
                curso = cbCursosDisponibles.SelectedItem as ECurso;

                if (dgvCursosDocente.RowCount > 0 && CursoRepetidos(curso))
                {
                    throw new Exception("El docente ya posee este curso");
                }
                listaDeCursos.Add(curso);
                dgvCursosDocente.DataSource = null;
                dgvCursosDocente.DataSource = listaDeCursos;

            }
            catch (Exception ex)
            {
                lblErrorD.Text = ex.Message;
            }

        }
        /// <summary>
        /// Indica Los colores De los DataGridView
        /// </summary>
        private void Colores()
        {
            dgvCursosDocente.ForeColor = Color.Black;
            dgvCursos.ForeColor = Color.Black;
            dgvDocentes.ForeColor = Color.Black;
            dgvHorarios.ForeColor = Color.Black;

        }

        private void btnCargaDgvD_Click(object sender, EventArgs e)
        {
            try
            {
                docente = dgvDocentes.CurrentRow.DataBoundItem as EDocente;
                listaDeCursos = docente.Cursos;
                CargarDatosDocente(docente);
            }
            catch (Exception ex)
            {

                lblErrorD.Text = ex.Message;
            }

        }

        private void btnCargaDgvH_Click(object sender, EventArgs e)
        {
            horario = dgvHorarios.CurrentRow.DataBoundItem as EHorario;
            CargarDatosHorario(horario);

        }

        private void btnCargaDgvC_Click(object sender, EventArgs e)
        {
            curso = dgvCursos.CurrentRow.DataBoundItem as ECurso;
            CargarDatosCurso(curso);
        }
        /// <summary>
        /// Carga los DATOS de los Cursos
        /// </summary>
        /// <param name="curso">Curso A Caragr</param>
        private void CargarDatosCurso(ECurso curso)
        {
            txtNombreC.Text = curso.Nombre;
            txtmAula.Text = curso.Aula;
            dtpInicio.Value = curso.FechaInicio;
            dtpFinal.Value = curso.FechaFinal;
            txtmCupo.Text = curso.Cupo.ToString();
            cbHorariosC.DataSource = null;
            cbHorariosC.DataSource = hbol.CargaTodos();
            foreach (EHorario obj in cbHorariosC.Items)
            {
                if (curso.Horario.Id == obj.Id)
                {
                    cbHorariosC.SelectedItem = obj;
                }
            }
        }
        /// <summary>
        /// Carga los DATOS de los Horarios
        /// </summary>
        /// <param name="curso">Horario A Caragr</param>
        private void CargarDatosHorario(EHorario horario)
        {
            cbDiaHorario.SelectedIndex = NumberIndexDia(horario.Dia);
            cbHoraInicial.SelectedIndex = NumberIndexHora(horario.HoraInicio);
            cbHoraFinal.SelectedIndex = NumberIndexHora(horario.HoraFinal);
        }
        /// <summary>
        /// Carga los DATOS de los Docentes
        /// </summary>
        /// <param name="curso">Docente A Caragr</param>
        private void CargarDatosDocente(EDocente docente)
        {

            txtmPinD.Text = docente.Pin.ToString();
            txtmCedD.Text = docente.Cedula;
            txtNombreD.Text = docente.Nombre;
            txtxApellidoD.Text = docente.Apellido;
            txtEmailD.Text = docente.Email;
            txtmTeleD.Text = docente.Telefono;
            listaDeCursos = docente.Cursos;
            dgvCursosDocente.DataSource = null;
            dgvCursosDocente.DataSource = listaDeCursos;
            cbCursosDisponibles.DataSource = cbol.CargarTodos();

        }
        /// <summary>
        /// Toma el Valor de una Hora para Convertirla en Index para El Combo Box
        /// </summary>
        /// <param name="horaFinal">String : Hora</param>
        /// <returns>int con el index</returns>
        private int NumberIndexHora(string horaFinal)
        {
            switch (horaFinal)
            {
                case "8:00":
                    return 0;
                case "9:00":
                    return 1;
                case "10:00":
                    return 2;
                case "11:30":
                    return 3;

                case "13:00":
                    return 5;
                case "14:00":
                    return 6;
                case "15:00":
                    return 7;
                case "16:30":
                    return 8;

                case "18:00":
                    return 10;
                case "19:00":
                    return 11;
                case "20:00":
                    return 12;
                case "21:00":
                    return 13;
                case "22:00":
                    return 14;
            }
            return 0;
        }
        /// <summary>
        /// Toma los Dias para convertirlos en Index de un Combo Box
        /// </summary>
        /// <param name="dia">String Dia</param>
        /// <returns>int32 Con el index</returns>
        private int NumberIndexDia(string dia)
        {
            switch (dia)
            {
                case "Lunes":
                    return 0;
                case "Martes":
                    return 1;
                case "Miercoles":
                    return 2;
                case "Jueves":
                    return 3;
                case "Viernes":
                    return 4;
                case "Lunes/Martes":
                    return 5;
                case "Lunes/Miercoles":
                    return 6;
                case "Lunes/Jueves":
                    return 7;
                case "Lunes/Viernes":
                    return 8;
                case "Martes/Miercoles":
                    return 9;
                case "Martes/Jueves":
                    return 10;
                case "Martes/Viernes":
                    return 11;
                case "Miercoles/Jueves":
                    return 12;
                case "Jueves/Viernes":
                    return 13;
            }
            return 0;
        }
        /// <summary>
        /// Valida si las Horas Son Validas Al ingresar un Horario.
        /// </summary>
        /// <param name="Inicial"></param>
        /// <param name="Final"></param>
        /// <returns></returns>
        private bool HorasValidas(string Inicial, string Final)
        {

            string[] auxI = Inicial.Split(':');
            string[] auxF = Final.Split(':');


            int horaI = Convert.ToInt32(auxI[0]);
            int horaF = Convert.ToInt32(auxF[0]);

            if (horaI < horaF)
            {
                return true;
            }
            else
            {
                throw new Exception("La Hora Inicial no puede " + "\n" + "ser mayor a la hora Final");
            }

        }

        private void cbHoraInicial_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbHoraInicial.Text.Equals("//Tarde//"))
            {
                cbHoraInicial.SelectedIndex = 5;
            }
            else if (cbHoraInicial.Text.Equals("//Noche//"))
            {
                cbHoraInicial.SelectedIndex = 10;
            }
        }

        private void cbHoraFinal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbHoraInicial.Text.Equals("//Tarde//"))
            {
                cbHoraInicial.SelectedIndex = 5;
            }
            else if (cbHoraInicial.Text.Equals("//Noche//"))
            {
                cbHoraInicial.SelectedIndex = 10;
            }
        }
        /// <summary>
        /// Limpia un Grupbox con sus Componentes
        /// </summary>
        /// <param name="groupBox">Gorup Box a Limpiar</param>
        private void Limpiar(GroupBox groupBox)
        {
            foreach (Control obj in groupBox.Controls)
            {
                // si tiene hijos, recórrelos de forma recursiva
                if (obj is TextBox) ((TextBox)obj).Text = "";
                if (obj is MaskedTextBox) ((MaskedTextBox)obj).Text = "";
                if (obj is DateTimePicker) ((DateTimePicker)obj).Value = DateTime.Now;
                if (obj is ComboBox) ((ComboBox)obj).SelectedIndex = 0;
                if (obj is DataGridView) ((DataGridView)obj).DataSource = null;
            }

        }
        /// <summary>
        /// Carga un Data Grid Viw con Los Cursos Necesarios
        /// </summary>
        /// <param name="dgv">El DataGridVie w</param>
        private void CargarCursos(DataGridView dgv)
        {
            dgv.DataSource = null;
            dgv.DataSource = cbol.CargarTodos();
        }
        /// <summary>
        /// Carga un Data Grid Viw con Los Cursos Necesarios
        /// </summary>
        /// <param name="dgv">El DataGridVie w</param>
        private void CargarDocentes(DataGridView dgv)
        {
            dgv.DataSource = null;
            dgv.DataSource = dbol.CargaTodos();
        }
        /// <summary>
        /// Carga un Data Grid Viw con Los Cursos Necesarios
        /// </summary>
        /// <param name="dgv">El DataGridVie w</param>
        private void CargarHorario(DataGridView dgv)
        {
            dgv.DataSource = null;
            dgv.DataSource = hbol.CargaTodos();
        }
        /// <summary>
        /// Le indica al Usuario si Ya ese Curso se encuentra en El Dgv.
        /// </summary>
        /// <param name="curso"></param>
        /// <returns></returns>
        private bool CursoRepetidos(ECurso curso)
        {
            foreach (DataGridViewRow row in dgvCursosDocente.Rows)
            {
                if (row.Cells[0].Value.ToString().Equals(curso.Id.ToString()))
                {
                    return true;
                }
            }
            return false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmHome frm = new FrmHome();
            frm.Visible = true;
            Hide();
        }
        /// <summary>
        /// Valida si Cupo es vacio y lo ingresa como 0
        /// </summary>
        /// <param name="txt">TXT cUPO </param>
        /// <returns></returns>
        private int IngresarCupo(string txt)
        {
            if (txt.Equals(""))
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(txt);
            }
        }

        private void btnElimiDgvH_Click(object sender, EventArgs e)
        {
            try
            {

                if (MessageBox.Show("Esta Seguro que desea Eliminar Este Horario ?", "Eliminar Horario",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    horario = dgvHorarios.CurrentRow.DataBoundItem as EHorario;
                    if (hbol.Remove(horario))
                    {
                        lblErrorH.Text = "Se elimino con Exito";
                        CargarHorario(dgvHorarios);
                    }
                }
            }
            catch (Exception)
            {

                lblErrorH.Text = "No se Elimino Correctamente el Horario";
            }
        }

        private void btnElimiDgvC_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Esta Seguro que desea Eliminar Este Curso ?", "Eliminar Curso",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    curso = dgvCursos.CurrentRow.DataBoundItem as ECurso;
                    if (cbol.Remove(curso))
                    {
                        lblErrorC.Text = "Se elimino con Exito";
                        CargarCursos(dgvCursos);
                    }
                }
            }

            catch (Exception)
            {

                lblErrorC.Text = "No se Elimino Correctamente el Horario";

            }
        }

        private void btnElimiDgvD_Click(object sender, EventArgs e)
        {
            try
            {


                if (MessageBox.Show("Esta Seguro que desea Eliminar Este Doncente ?",
                    "Eliminar Docente",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    docente = dgvDocentes.CurrentRow.DataBoundItem as EDocente;
                    if (dbol.Remove(docente))
                    {
                        lblErrorD.Text = "Se elimino con Exito";
                        CargarDocentes(dgvDocentes);
                    }
                }
            }
            catch (Exception)
            {

                lblErrorD.Text = "No se Elimino Correctamente el Horario";

            }
        }

        private void dgvHorarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnNuevoC_Click(object sender, EventArgs e)
        {
            curso = new ECurso();
            CargarDatosCurso(curso);
        }

        private void btnNuevoD_Click(object sender, EventArgs e)
        {
            docente = new EDocente();
            CargarDatosDocente(docente);
        }

        private void btnNuevoH_Click(object sender, EventArgs e)
        {
            horario = new EHorario();
            CargarDatosHorario(horario);
        }

        private void dgvCursosDocente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            ECurso aux = dgvCursosDocente.CurrentRow.DataBoundItem as ECurso;
            listaDeCursos.Remove(aux);
            dgvCursosDocente.DataSource = null;
            dgvCursosDocente.DataSource = listaDeCursos;
        }
    }
}
