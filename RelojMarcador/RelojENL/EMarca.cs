﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RelojMarcadorENL
{
    public class EMarca
    {
        public int Id { get; set; }
        public int Id_Docente { get; set; }
        public int Entrada { get; set; }
        public string HoraDeEntrada { get; set; }
        public string Introduccion { get; set; }
        public int Salida { get; set; }
        public string HoraSalida { get; set; }
        public string Justificacion { get; set; }
        public DateTime Dia { get; set; }
    }
}
