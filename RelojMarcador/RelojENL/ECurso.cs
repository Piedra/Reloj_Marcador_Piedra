﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RelojMarcadorENL
{
    public class ECurso
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Aula { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFinal { get; set; }
        public int Cupo { get; set; }
        public EHorario Horario { get; set; }
        public int Activo { get; set; }

        public override string ToString()
        {
            return "Nombre: " + Nombre + " Aula: " + Aula + " Horario "+ Horario;
        }

    }
}
