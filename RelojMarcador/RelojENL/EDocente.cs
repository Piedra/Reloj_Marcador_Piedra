﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RelojMarcadorENL
{
    public class EDocente
    {
        public int Id { get; set; }
        public int Pin { get; set; }
        public string Cedula { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public int CantidadT { get; set; }
        public int CantidadA { get; set; }
        public int Activo { get; set; }
        public List<ECurso> Cursos { get; set; }
    }
}
