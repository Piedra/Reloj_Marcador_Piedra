﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RelojMarcadorENL
{
    public class EHorario
    {
        public int Id { get; set; }
        public string Dia { get; set; }
        public string HoraInicio { get; set; }
        public string HoraFinal { get; set; }
        public int Activo { get; set; }

        public override string ToString()
        {
            return "Dia: " + Dia + " HI: " + HoraInicio + " HF: " + HoraFinal;
        }

    }

    
}
